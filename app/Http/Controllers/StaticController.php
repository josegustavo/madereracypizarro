<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

class StaticController extends Controller
{
    protected $lang;

    protected $params = [];

    public function __construct()
    {
        $route = app('request')->route();
        $this->lang = $route[1]['as']??'es';
        trans()->setLocale($this->lang);

        $names = ['menus','us','contact_us'];
        $this->load_params($names);
    }

    protected function get_table($table)
    {
        $services_db = app('db')->table($table)->get();
        $values = [];
        foreach ($services_db as $serv){
            $values[$serv->name] = json_decode($serv->value);
        }
        return $values;

    }

    protected function load_params($names)
    {
        $params = app('db')->table('params')->whereIn('name', $names)->get();
        foreach($params as $param)
        {
            $this->params[$param->name] = json_decode($param->value);
        }
    }

    public function home()
    {
        $this->load_params(['mainslider','intros','welcome','what_we_do','products_home']);
        
        return view('home', $this->params);
    }

    public function us()
    {
        return view('us', $this->params);
    }

    public function products()
    {
        $this->load_params(['products']);
        $this->params['products_list'] = $this->get_table('products');
        return view('products', $this->params);
    }

    public function product($product_slug)
    {
        $product = app('db')->table('products')->where('name', $product_slug)->first();
        if(!$product)
        {
            if(!app('auth')->guest()){
                app('db')->table('products')->insert([
                    'name' => $product_slug,
                    'value' => json_encode([
                        "title" => $product_slug,
                        "slug" => $product_slug,
                        "action" => ["url" => "/productos/$product_slug"]
                    ]),
                ]);
                $product = app('db')->table('products')->where('name', $product_slug)->first();
            }
            else
                abort(404, 'No record found');
        }

        $this->load_params(['products']);
        $this->params['product_slug'] = $product_slug;
        $products_list = $this->get_table('products');
        unset($products_list[$product_slug]);
        $this->params['products_list'] = $products_list;
        $this->params['product'] = json_decode($product->value);

        return view('product', $this->params);
    }

    public function services()
    {
        $this->load_params(['services']);
        $this->params['services_list'] = $this->get_table('services');
        return view('services', $this->params);
    }

    public function service($service_slug)
    {
        $service = app('db')->table('services')->where('name', $service_slug)->first();
        if(!$service)
        {
            if($service_slug != str_slug($service_slug))
                redirect("/servicios/".str_slug($service_slug));

            if(!app('auth')->guest()){
                app('db')->table('services')->insert([
                    'name' => $service_slug,
                    'value' => json_encode(["title" => $service_slug]),
                ]);
                $service = app('db')->table('services')->where('name', $service_slug)->first();
            }
            else
                abort(404, 'No service found');
        }
        $this->load_params(['services']);
        $this->params['service_slug'] = $service_slug;
        $this->params['service'] = json_decode($service->value);
        $this->params['services_list'] = $this->get_table('services');
//dd($this->params['services_list']);
        return view('service', $this->params);
    }

    public function sustentability()
    {
        $this->load_params(['sustentability']);
        return view('sustentability', $this->params);
    }

    public function contact()
    {
        $this->load_params(['contact_us']);
        return view('contact', $this->params);
    }

    public function  sendmail(Request $request)
    {
        $data = (object)$request->all();

        $name = $data->name??null;
        if(!$name) abort(401, 'Debe ingresar un nombre');
        
        $from = $data->email??null;
        if(!$from) abort(401, 'Debe ingresar un email');
        if (!filter_var($from, FILTER_VALIDATE_EMAIL)) abort(401, 'El email ingresado no es válido');

        $message = $data->message??null;
        if(!$message) abort(401, 'Debe ingresar un mensaje');

        $file = ($request->attach)?$request->attach:NULL;

        $mailto = 'luther1911@gmail.com';
        $from_name = 'Maderera S&C pizarro Website';
        $subject = 'Contacto desde página web madererasycpizarro.com';
        $from_mail = 'no-reply@madererasycpizarro.com';
        $replyto = $from;
        if($file)
        {
            $filename = $file->getClientOriginalName();
            $file_path = $file->path();
            $file_size = filesize($file_path);
            $handle = fopen($file_path, "r");
            $content = fread($handle, $file_size);
            fclose($handle);
            $content = chunk_split(base64_encode($content));
        }
        $eol = PHP_EOL;
        $uid = md5(uniqid(time()));
        $header = "From: ".$from_name." <".$from_mail.">".$eol;
        $header .= "Reply-To: ".$name." <".$replyto.">".$eol;
        $header .= "MIME-Version: 1.0".$eol;
        $header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"".$eol;
        $header .= "This is a multi-part message in MIME format.".$eol;
        $msg = "--".$uid.$eol;
        $msg .= "Content-type:text/plain; charset=ISO-8859-1".$eol;
        $msg .= "Content-Transfer-Encoding: 8bit".$eol;
        $msg .= $message.$eol;
        $msg .= "--".$uid.$eol;
        if($file) {
            $msg .= "Content-Type: application/octet-stream; name=\"" . $filename . "\"".$eol; // use different content types here
            $msg .= "Content-Transfer-Encoding: base64".$eol;
            $msg .= "Content-Disposition: attachment; filename=\"" . $filename . "\"".$eol;
            $msg .= $content . $eol;
            $msg .= "--" . $uid . "--";
        }
        $send = mail($mailto, $subject, $msg, $header);

        return redirect('contacto');

    }
}
