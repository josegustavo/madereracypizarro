<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Symfony\Component\HttpFoundation\Cookie;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function home(Request $request) 
    {
        $params = [
            'menus' => [
                [
                    'name' => 'Inicio',
                    'url' => '/'
                ],
                [
                    'name' => 'Configuración',
                    'url' => '/admin'
                ],
                [
                    'name' => 'Traducción',
                    'url' => '/admin/translate'
                ],
                [
                    'name' => 'Salir',
                    'url' => '/logout'
                ]
            ]
        ];
        return view('admin', $params);
    }


    public function save(Request $request)
    {
        $req = $request->all();
        $nested = explode('.', $req['name']);
        $table = array_shift($nested);
        $nested = implode('.', $nested);
        $keys = explode('.', $nested);
        $value = $req['value']??NULL;   
        
        $group = array_shift($keys);

        
        if($value instanceof \Illuminate\Http\UploadedFile){
            $basename = implode('_',$keys).'.'.$value->guessClientExtension();
            $value->move(base_path('public/images/'.$table.'/'.$group), $basename);
            $value = $table.'/'.$group.'/'.$basename.'?t='.time();
        }
        
        $param = [
            $group => json_decode(app('db')->table($table)->where('name', $group)->value('value'), TRUE)
        ];
        
        if($value === NULL){
            if(count(explode(".", $nested)) === 1){
                app('db')->table($table)->where('name', $group)->delete();
                return $value;
            }
            else
                array_forget($param, $nested);
        }else
            data_set($param, $nested, $value);
        
        if($result = app('db')->table($table)->where('name', $group)->update(['value' => json_encode($param[$group])]))
            return $value;
        else
            return [$result];
    }

}
