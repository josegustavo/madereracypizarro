<?php


if (! function_exists('__')) {
    function __($id = null, $replace = [], $locale = null)
    {
        return trans()->getFromJson($id, $replace, $locale);
    }
}


