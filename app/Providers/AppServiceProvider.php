<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }


    public function boot()
    {
        Blade::directive('editable', function ($expression) {
            $args = explode(',',$expression);
            foreach($args as $arg){
                $keys []= ($arg[0]==='$')?"'.$arg.'":trim($arg,"'");
            }
            $keys = implode('.', $keys);
            return "<?=(!app('auth')->guest())?('edit-key=\"$keys\"'):''?>";
        });
        Blade::directive('repeater', function ($expression) {
            $args = explode(',',$expression);
            foreach($args as $arg){
                $keys []= ($arg[0]==='$')?"'.$arg.'":trim($arg,"'");
            }
            $keys = implode('.', $keys);
            return "<?=(!app('auth')->guest())?('repeater-add=\"$keys\"'):''?>";
        });
        Blade::directive('repeateritem', function ($expression) {
            $args = explode(',',$expression);
            foreach($args as $arg){
                $keys []= ($arg[0]==='$')?"'.$arg.'":trim($arg,"'");
            }
            $keys = implode('.', $keys);
            return "<?=(!app('auth')->guest())?('repeater-item=\"$keys\"'):''?>";
        });
        Blade::directive('editableimg', function ($expression) {
            $args = explode(',',$expression);
            $keys = [];
            foreach($args as $arg){
                $keys []= ($arg[0]==='$')?"'.$arg.'":(($arg[0]==='@')?"'.@$arg.'":trim($arg,"'"));
            }
            $keys = implode('.', $keys);
            return "<?=(!app('auth')->guest())?('edit-image=\"$keys\"'):''?>";
        });

        Blade::directive('auth', function($expression){
            return '<?php if(!app("auth")->guest()){ ?>';
        });
        
        Blade::directive('endauth', function($expression){
            return '<?php } ?>';
        });
    }
}
