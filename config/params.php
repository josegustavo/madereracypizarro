<?php

return [
    
    'contact_us' => [
        'title' => 'Contáctenos',
        'description' => 'Agradecemos su vista a nuestro sitio web. Si desea comunicarse con nosotros o realizar alguna consulta lo puede realizar mediante la siguiente información.',
        'email' => [
            'contacto@madererapizarro.com'
        ],
        'phones' => [
            '(+51) 01 283-3693',
            '(+51) 9950 28229',
            '(+51) 9901 90191'
        ],
        'social' => [//Activar redes sociales            
            'facebook' => 'http://facebook.com/MadereraPizarro',
            'twitter' => 'http://twitter.com/MadereraPizarro',
        ],
        'address' => [
            'Oficina Principal' => 'Av. Pachacútec 1779 - Villa María del Triunfo Lima 35 - Lima - Perú (Referencia: A 4 cuadras del Hospital María Auxiliadora – Estación María Auxiliadora Línea 1)',
            'Oficina Sucursal' => 'Av. Pachacútec 1805 cruce Jr. Miguel Grau - Villa María del Triunfo Lima 35 - Lima - Perú (Referencia: A 5 cuadras del Hospital María Auxiliadora – Estación María Auxiliadora Línea 1)',
        ],
        'open_hours' => 'Lunes a Sábado de 8:00 a.m. - 6:30 p.m.',
        'copy' => 'Copyright © 2018 Maderera Pizarro. Todos los derechos reservados.',
        'iframe_map' => 'https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d868.6809925515792!2d-76.95368049239025!3d-12.163670106138522!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9105b8fb231a08e1%3A0x4f1629d127cddf35!2sMaderera%20SyC%20Pizarro!5e0!3m2!1ses-419!2spe!4v1571077956953!5m2!1ses-419!2spe',
    ],    
    
	'intros' => [
		[
			'title' => 'Cortes especiales',
			'description' => 'Ofrecemos una variedad de cortes de acuerdo a las necesidades del cliente.',
			'icon' => 'icons/cortes.png'
		],
		[
			'title' => 'Despacho a tiempo', 
			'description' => 'Nuestra eficiencia y experiencia nos permiten realizar despachos a tiempo.',
			'icon' => 'icons/time.png'
		],
		[
			'title' => 'Atención personalizada', 
			'description' => 'Atención rápida y confiable nos diferencian, debido a que resolvemos tus necesidades.',
			'icon' => 'icons/buoy.png'
		],
	],
    
    'welcome' => [
        'intro' => 'Here we are',
        'title' => 'Welcome!',
        'slides' => [
            [
                'title' => 'Responsible Forestry. Quality Timber',
                'description' => 'We market forest products and provide land management in many counties.',
                'action_text' => 'view products',
                'action_url' => '#',
                'image' => 'maderas/capirona.jpg'
            ],
            [
                'title' => 'Responsible Forestry. Quality Timber',
                'description' => 'We market forest products and provide land management in many counties.',
                'action_text' => 'view products',
                'action_url' => '#',
                'image' => 'maderas/cashimbo.jpg'
            ],
            [
                'title' => 'Responsible Forestry. Quality Timber',
                'description' => 'We market forest products and provide land management in many counties.',
                'action_text' => 'view products',
                'action_url' => '#',
                'image' => 'maderas/catahua.jpg'
            ],
        ]
    ],

    'what_we_do' => [
        'intro' => 'About us',
        'title' => 'What we do!',
        'action_text' => 'view products',
        'action_url' => '#',
        'slides' => [
            [
                'title' => 'LOGGING',
                'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Praesent vestibulum molestie lacus. Aenean nonummy hendrerit mauris.',
                'action_url' => '#',
                'image' => 'servicios1.jpg'
            ],
            [
                'title' => 'harvesting',
                'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Praesent vestibulum molestie lacus. Aenean nonummy hendrerit mauris.',
                'action_url' => '#',
                'image' => 'servicios2_1.jpg'
            ],
            [
                'title' => 'manufacturing',
                'description' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Praesent vestibulum molestie lacus. Aenean nonummy hendrerit mauris.',
                'action_url' => '#',
                'image' => 'servicios3_1.jpg'
            ]
        ]
    ],
    
    //Verificar que las imágenes existan en la carpeta images/
    //Recomendado el uso de imágenes de 1170x400 y que no pesen mucho
    'productos' => [
        'content' => [
            [
                'title' => 'Maderas',
                'description' => [
                    'En Maderera Pizarro contamos con más de 30 años de experiencia en la transformación de la madera y sus actividades relacionadas como el aserrío y cepillado de la misma.',
                    'Además, se ofrece una variedad de especies de madera aserrada, cepillada y machihembrada, para atender y satisfacer las distintas necesidades de nuestros clientes poniendo gran atención en la calidad del producto en las etapas de su procesamiento.',
                ],
                'image' => 'maderas1.jpg',
            ],
            [
                'title' => 'Especificación Técnica',
                'description' => [
                    'Las maderas cuentan con una especificación de acuerdo a las dimensiones que poseen. Las dimensiones básicas son tres: largo, ancho y espesor en unidades de longitud.',
                    'Es importante conocer y establecer las dimensiones de las maderas para la solicitud y compra de las mismas, respectivamente.'
                ],
                'image' => 'maderas2.jpg'
            ]
        ],
        'maderas' => [
            'capirona' => [
                'name' => 'Capirona',
                'image' => 'maderas/capirona.jpg',
                'icon' => 'maderas/icons/capirona.jpg',
                'slide' => 'maderas/slides/capirona.jpg',
                'texture' => 'maderas/texturas/capirona.jpg',
                'description' => 'descripción de madera capirona',
                'details' => [
                    'Nombres' => [
                        'Nombre en inglés' => 'Capirona',
                        'Sinónimos internacionales' => 'Pau mulato',
                        'Nombre científico' => 'Calycophyllum spruceanum',
                        'Nombres comunes' => 'Capirona, Pau Mulato',
                        'Familia' => 'Rubiaceae',
                        'Origen' => 'Tropical S/A',
                    ],
                    'Descripción de la madera' => [
                        'Color' => 'blanco pardo',
                        'Grano' => 'recto a entrecruzado',
                        'Textura' => 'muy fina',
                        'Brillo' => 'medio intenso',
                        'Vetado' => 'poco diferenciado'
                    ],
                    'Características de procesamiento y uso' => [
                        'Comportamiento al secado artificial' => 'bueno',
                        'Durabilidad natural' => 'alta',
                        'Trabajabilidad' => 'bueno',
                        'Uso' => 'vigas, columnas, pisos, mueblería molduras, artesanías, mangos de herramientas entre otros',
                    ],
                    'Propiedades físicas y mecánicas' => [
                        'Densidad básica' => '760 kg/m3'
                    ]
                ]
            ],
            'cashimbo' => [
                'name' => 'Cashimbo',
                'image' => 'maderas/cashimbo.jpg',
                'slide' => 'maderas/slides/cashimbo.jpg',
                'texture' => 'maderas/texturas/cashimbo.jpg',
                'icon' => 'maderas/icons/cashimbo.jpg',
                'description' => 'descripción de madera cashimbo',
                'details' => [
                    'Nombres' => [
                        'Nombre en inglés' => 'Jekitiba',
                        'Sinónimos internacionales' => 'Bacú',
                        'Nombre científico' => 'Cariniana spp.',
                        'Nombres comunes' => 'Cashimbo Cachimbo',
                        'Familia' => 'Lecythidaceae',
                        'Origen' => 'Tropical S/A',
                    ],
                    'Descripción de la madera' => [
                        'Color' => 'Rosado cremoso',
                        'Grano' => 'de recto a entrecruzado',
                        'Textura' => 'media',
                        'Brillo' => 'bajo a mediano',
                        'Vetado' => 'Satinado poco pronunciado',
                    ],
                    'Características de procesamiento y uso' => [
                        'Comportamiento al secado artificial' => 'bueno',
                        'Durabilidad natural' => 'mediana',
                        'Trabajabilidad' => 'bueno',
                        'Uso' => 'Carpinteria interior, contra chapado, molduras, construcción ligera, mobiliario corriente, embalaje, juguetes, entarimados',
                    ],
                    'Propiedades físicas y mecánicas' => [
                        'Densidad básica' => '590 kg/m3'
                    ]
                ]
            ],
            'catahua' => [
                'name' => 'Catahua',
                'image' => 'maderas/catahua.jpg',
                'icon' => 'maderas/icons/catahua.jpg',
                'texture' => 'maderas/texturas/catahua.jpg',
                'description' => 'descripción de madera catahua',
                'details' => [
                    'Nombres' => [
                        'Nombre en inglés' => 'Assacu',
                        'Sinónimos internacionales' => 'Mura, Acacu, Possum',
                        'Nombre científico' => 'Hura crepitans L.',
                        'Nombres comunes' => 'Catahua',
                        'Familia' => 'Euphorbiaceae',
                        'Origen' => 'Tropical S/A',
                    ],
                    'Descripción de la madera' => [
                        'Color' => 'blanca amarillenta',
                        'Grano' => 'recto a entrecruzado',
                        'Textura' => 'fina',
                        'Brillo' => 'Alto',
                        'Vetado' => 'poco diferenciado',
                    ],
                    'Características de procesamiento y uso' => [
                        'Comportamiento al secado artificial' => 'bueno',
                        'Durabilidad natural' => 'moderado',
                        'Trabajabilidad' => 'bueno',
                        'Uso' => 'Cajoneria , revestimiento interior, chapas y contrachapado, material de relleno, muebleria barata y ebanisteria, embalaje de productos prerecibles, triplay donde se requiere una madera liviana y facil de trabajar.',
                    ],
                    'Propiedades físicas y mecánicas' => [
                        'Densidad básica' => '0.41 gr/cm3'
                    ]
                ]
            ],
            'copayba' => [
                'name' => 'Copayba',
                'image' => 'maderas/copayba.jpg',
                'icon' => 'maderas/icons/copayba.jpg',
                'texture' => 'maderas/texturas/copayba.jpg',
                'description' => 'descripción de madera copayba',
                'details' => [
                    'Nombres' => [
                        'Nombre en inglés' => 'Copaiba',
                        'Sinónimos internacionales' => 'Copaifera jacquini Desf; Copaiba officinalis Adans. ; Copaiva officinalis Jacq',
                        'Nombre científico' => 'Copaifera officinalisL.',
                        'Nombres comunes' => 'Copaiba, aceite cabimo, cabima',
                        'Familia' => 'Caesalpinioideae',
                        'Origen' => 'Tropical S/A',
                    ],
                    'Descripción de la madera' => [
                        'Color' => ' blanco rosáceo',
                        'Grano' => 'Recto',
                        'Textura' => 'Media  a fina',
                        'Brillo' => 'Medio',
                        'Vetado' => 'Arcos superpuestos y bandas longitudinales muy angostas y oscuras',
                    ],
                    'Características de procesamiento y uso' => [
                        'Comportamiento al secado artificial' => 'bueno',
                        'Durabilidad natural' => 'alta',
                        'Trabajabilidad' => 'bueno',
                        'Uso' => 'Vigas, columnas, machihembrados, muebles y objetos torneados. También se utiliza en carpintería, pisos , revestimiento interiores, parquet, contra chapado, entarimado,  elaboración de cajas, molduras, encofrados y laminados, por sus cualidades podría sustituir al Pino Oregon. Preservada podria utilizarse para estantillos o postes para cercas. Es apta para tableros de partículas y tableros madera - cemento.',
                    ],
                    'Propiedades físicas y mecánicas' => [
                        'Densidad básica' => '0.61 gr/cm3'
                    ]
                ]
            ],
            'huayruro' => [
                'name' => 'Huayruro',
                'image' => 'maderas/huayruro.jpg',
                'icon' => 'maderas/icons/huayruro.jpg',
                'texture' => 'maderas/texturas/huayruro.jpg',
                'description' => 'descripción de madera huayruro',
                'details' => [
                    'Nombres' => [
                        'Nombre en inglés' => 'Angelim pedra',
                        'Sinónimos internacionales' => '',
                        'Nombre científico' => 'Ormosia macrocarpia',
                        'Nombres comunes' => 'Angelim pedra',
                        'Familia' => 'Fabaceae leguminosae',
                        'Origen' => 'Tropical S/A',
                    ],
                    'Descripción de la madera' => [
                        'Color' => 'marrón claro, y amarillo a rojizo',
                        'Grano' => 'entrecruzado',
                        'Textura' => 'gruesa',
                        'Brillo' => 'medio',
                        'Vetado' => 'arcos superpuestos en corte tangencial y bandas paralelas',
                        'Olor' => 'poco perceptible',
                    ],
                    'Características de procesamiento y uso' => [
                        'Comportamiento al secado artificial' => 'bueno',
                        'Durabilidad natural' => 'media - alta',
                        'Trabajabilidad' => 'muy bueno',
                        'Uso' => 'estructuras como vigas, columnas, tijerales, mueblería, pisos, marco de puertas y ventanas, entre otros',
                    ],
                    'Propiedades físicas y mecánicas' => [
                        'Densidad básica' => '610 kg/m3 medio - alto'
                    ]
                ]
            ],
            'roble' => [
                'name' => 'Roble',
                'image' => 'maderas/roble.jpg',
                'icon' => 'maderas/icons/roble.jpg',
                'texture' => 'maderas/texturas/roble.jpg',
                'description' => 'descripción de madera roble',
                'details' => [
                    'Nombres' => [
                        'Nombre en inglés' => 'Cerejeira',
                        'Sinónimos internacionales' => 'Roble Boliviano, Ishpingo',
                        'Nombre científico' => 'Amburana cearensis',
                        'Nombres comunes' => 'Roble boliviano, Cerejeira',
                        'Familia' => 'Fabaceae',
                        'Origen' => 'Tropical S/A',
                    ],
                    'Descripción de la madera' => [
                        'Color' => 'amarillo a marrón',
                        'Grano' => 'entrecruzado',
                        'Textura' => 'media a gruesa',
                        'Brillo' => 'medio',
                        'Vetado' => 'arcos superpuestos y bandas',
                        'Olor' => 'característico',
                    ],
                    'Características de procesamiento y uso' => [
                        'Comportamiento al secado artificial' => 'muy bueno',
                        'Durabilidad natural' => 'media - alta',
                        'Trabajabilidad' => 'muy buena',
                        'Uso' => 'mueblería en general, laminados, puertas, ventanas y otros',
                    ],
                    'Propiedades físicas y mecánicas' => [
                        'Densidad básica' => '430 kg/m3 media'
                    ]
                ]
            ],
            'tornillo' => [
                'name' => 'Tornillo',
                'image' => 'maderas/tornillo.jpg',
                'icon' => 'maderas/icons/tornillo.jpg',
                'texture' => 'maderas/texturas/tornillo.jpg',
                'description' => 'descripción de madera tornillo',
                'details' => [
                    'Nombres' => [
                        'Nombre en inglés' => 'Tornillo',
                        'Sinónimos internacionales' => 'Cedro arana',
                        'Nombre científico' => 'Cedrelinga catenaeformis',
                        'Nombres comunes' => 'Tornillo',
                        'Familia' => 'Mimosaceae',
                        'Origen' => 'Tropical S/A',
                    ],
                    'Descripción de la madera' => [
                        'Color' => 'marrón claro',
                        'Grano' => 'entrecruzada',
                        'Textura' => 'gruesa',
                        'Brillo' => 'medio',
                        'Vetado' => 'poco diferenciado',
                        'Olor' => 'característico',
                    ],
                    'Características de procesamiento y uso' => [
                        'Comportamiento al secado artificial' => 'muy bueno',
                        'Durabilidad natural' => 'alta',
                        'Trabajabilidad' => 'muy buena',
                        'Uso' => 'estructural, como vigas columnas, tijerales y carpinterí­a en general',
                    ],
                    'Propiedades físicas y mecánicas' => [
                        'Densidad básica' => '450 kg/m3 medio'
                    ]
                ]
            ],
        ]
    ],
    
    //Se mostrarán solo los 3 primeros servicios
    'servicios' => [
        'corte' => [
            'shortname' => 'Cortes',
            'name' => 'Corte o Tableado de Madera',
            'description' => 'Maderera Pizarro ofrece diversos servicios para el tratado de la madera y/o producto...',
            'content' => [
                'Maderera Pizarro ofrece diversos servicios para el tratado de la madera y/o producto. El corte de la madera consiste en la transformación de una troza de madera a un producto con dimensiones específicas de ancho, largo y espesor, con el fin de ser utilizado en un proceso posterior, como lo es la fabricación de muebles, casas, entre otros.',
                'Al realizarse el corte de las trozas se producen adicionalmente polvos o pequeños granos de madera (aserrín).'
            ],
            'images' => [
                'servicios1.jpg',
            ]
        ],
        'cepillado' => [
            'shortname' => 'Cepillado',
            'name' => 'Cepillado de Madera',
            'description' => 'Además del tableado de maderas, Maderera Pizarro ofrece el servicio de cepillado...',
            'content' => [
                'Además del tableado de maderas, Maderera Pizarro ofrece el servicio de cepillado. El cepillado de la madera consiste en quitar las irregularidades del producto y emparejar la superficie de la madera. Diversas son las herramientas que se emplean y si bien todas cumplen la misma función, es decir, sacar laminillas de madera (viruta), no pueden utilizarse indistintamente.',
            ],
            'images' => [
                'servicios2_1.jpg',
                'servicios2_2.jpg'
            ]
        ],
        'machihembrado' => [
            'shortname' => 'Machihembrado',
            'name' => 'Machihembrado de Madera',
            'description' => 'El machihembrado es un servicio de ensamble de tablas de madera cepillada...',
            'images' => [
                'servicios3_1.jpg' => [
                    'El machihembrado es un servicio de ensamble de tablas de madera cepillada por medio de rebajes y cortes en sus cantos, para lograr por medio de la sucesión de piezas encajadas entre sí una sola superficie lisa, uniforme y sólida.'
                ],
                'servicios3_2.jpg' => [
                    'De este modo, se labra en los cantos de la tabla dos tipos de perfilado: macho, en forma de pestaña sobresaliente, y hembra, en forma de canal; sus medidas están pensadas para lograr una unión perfecta.',
                    'Para ensamblar las tablas, se encaja el canto cortado en macho de una pieza dentro del canto cortado en hembra de otra pieza, quedando unidas para soportar las cargas propias del uso.'
                ],
                'servicios3_3.jpg' => [
                    'Este sistema es utilizado principalmente para pisos de madera, donde se busca lograr una superficie lisa e indeformable frente a la aplicación de las cargas del uso. Para tablas largas (de 3,20 m de longitud) se labran sólo dos de los cuatro cantos, pues el dimensionamiento en obra de las piezas no justifica labrar los cuatro. ',
                    'Donde sí se da el uso de machihembrado en los cuatro cantos es en palmetas para parqué, entre otros.'
                ]
            ]
        ],
        'transporte' => [
            'shortname' => 'Transporte',
            'name' => 'Transporte de carga',
            'description' => 'Maderera Pizarro ofrece el servicio de transporte de madera o carga...',
            'content' => [
                'Maderera Pizarro ofrece el servicio de transporte de madera o carga. Puede consultar precios y cobertura de este servicio en los medios de contacto de la maderera.',
            ],
            'image' => 'servicios4.jpg',
        ]
    ],
    'sostenibilidad' => [
        'content' => [
            'Maderera Pizarro busca hacer un uso sostenible de la materia prima como es la madera, evitando y reduciendo los impactos negativos de nuestras operaciones y asegurar su capacidad productiva a futuro.',
            'Siendo la forestería la actividad que menos impacto genera en el suelo amazónico, si se realiza adecuadamente permitiría explotar un recurso renovable sin comprometer las necesidades de futuras generaciones, y así generar permanente riqueza en las zonas de la región.',
            'Al realizar las diversas actividades se pretende asegurar el cumplimiento de los derechos de nuestros colaboradores y ser un miembro activo en el desarrollo de los mismos. Por otro lado, ofrecer soluciones de calidad a nuestros clientes y promover el manejo sostenible de los bosques a nivel nacional e internacional procurando la continuidad de la empresa y sostenibilidad en el tiempo.',
            'Adicionalmente, se debe considerar que menos del 30% de los bosques de producción permanente están puestos en valor. Esto demuestra que la forestería aún está muy por debajo de su potencial, de acuerdo a las cuales la actividad representa menos del 1% de la economía en el Perú.'
        ],
        'image' => 'sostenibilidad.jpg'
    ],    
    'news' => [
        [
            'day' => '7',
            'month' => 'JUL',
            'title' => 'Lanzamiento del sitio web MadereraPizarro.com 2015',
            'subtitle' => 'Bienvenidos a la nueva página web',
            'content' => "Descripción de noticia 7 Julio 2015"
        ],
    ],
    
    'menus' => [
        [
            'name' => 'Inicio',
            'url' => '',
        ],
        [
            'name' => 'Nosotros',
            'url' => 'nosotros',
            'submenu' => [
                [
                    'name' => '¿Quiénes somos?',
                    'url' => '#quienes-somos'
                ],
                [
                    'name' => 'Misión y Visión',
                    'url' => '#mision-y-vision'
                ]
            ]
        ],
        [
            'name' => 'Productos',
            'url' => 'productos',
            'submenu' => [
                [
                    'name' => 'Maderas',
                    'url' => '#maderas'
                ]
            ]
        ],
        [
            'name' => 'Maderas',
            'url' => 'productos',
            'submenu' => [
                [
                    'name' => 'Capirona',
                    'url' => '/capirona'
                ],
                [
                    'name' => 'Cashimbo',
                    'url' => '/cashimbo'
                ],
                [
                    'name' => 'Catahua',
                    'url' => '/catahua'
                ],
                [
                    'name' => 'Copayba',
                    'url' => '/copayba'
                ],
                [
                    'name' => 'Huayruro',
                    'url' => '/huayruro'
                ],
                [
                    'name' => 'Roble',
                    'url' => '/roble'
                ],
                [
                    'name' => 'Tornillo',
                    'url' => '/tornillo'
                ],
            ]
        ],
        [
            'name' => 'Servicios',
            'url' => 'servicios',
            'submenu' => [
                [
                    'name' => 'Corte o Tableado de Madera',
                    'url' => '#corte'
                ],
                [
                    'name' => 'Cepillado de Madera',
                    'url' => '#cepillado'
                ],
                [
                    'name' => 'Machihembrado de Madera',
                    'url' => '#machihembrado'
                ],
                [
                    'name' => 'Transporte de Carga',
                    'url' => '#transporte'
                ],
            ]
        ],
        [
            'name' => 'Sostenibilidad',
            'url' => 'sostenibilidad',
        ],
        [
            'name' => 'Contacto',
            'url' => 'contacto',
            'submenu' => [
                [
                    'name' => '¿Dónde nos encontramos?',
                    'url' => '#donde-nos-encontramos'
                ],
                [
                    'name' => 'Envíenos un mensaje',
                    'url' => '#envienos-un-mensaje'
                ]
            ]
        ],
    ],

    'us' => [
        'title' => 'Nosotros',
        'sections' => [
            [
                'title' => '¿Quiénes somos?',
                'content' => [
                    'Maderera Pizarro es un grupo empresarial con más de 30 años de experiencia en el rubro maderero realizando actividades de transformación y distribución de productos de maderas finas en lima y provincias del Perú.',
                    'En el sector maderero, somos proveedores y comercializadores, contamos con una amplia variedad de especies de madera y ofrecemos diversos servicios de aserrío, cepillado, machihembrado, entre otros, de modo que se pueda atender y satisfacer las distintas necesidades de nuestros clientes poniendo gran atención en la calidad del producto en sus diferentes etapas.',
                    'Desde nuestros inicios nos hemos caracterizado por mantener altos niveles de calidad en nuestros productos y servicios. Por ello, hemos generado relaciones de largo plazo con nuestros clientes y proveedores, convirtiéndonos en una de las principales organizaciones comercializadoras de productos de madera en el Perú.',
                ],
                'image' => 'nosotros1.jpg'
            ],
            [
                'title' => 'Misión y Visión',
                'content' => [
                    'Misión: Satisfacer las necesidades más exigentes de nuestros clientes, brindándoles plena excelencia en los productos y servicios desde la transformación, comercialización local, nacional; de todo tipo de maderas a precios sumamente competitivos, teniendo como principio básico: Productos y servicios de excelencia con un estricto control de calidad.',
                    'Visión: Llegar a ser el principal grupo maderero del Perú ubicándonos a la vanguardia de las más grandes exigencias del rubro maderero; diversificando nuestros productos y servicios a nuestros clientes y proveedores así como manteniendo principios de conservación ambiental de los recursos naturales.',
                ],
                'image' => 'nosotros2.jpg',
            ]
        ]
    ],
];