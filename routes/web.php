<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
$appLangs = explode(',',env('APP_LANGS', ''));

foreach ($appLangs as $lang) 
{
    $prefix = ($lang !== env('APP_LOCALE'))?$lang:'';
    
    $router->group(['prefix' => $prefix, 'as' => $lang], function ($router) use ($lang) {


        //Home
        $router->get('/', 'StaticController@home');
        
        $router->get('/nosotros', 'StaticController@us');
        $router->get('/productos', 'StaticController@products');
        $router->get('/productos/{product_id}', 'StaticController@product');
        $router->get('/servicios', 'StaticController@services');
        $router->get('/servicios/{service_slug}', 'StaticController@service');
        $router->get('/sostenibilidad', 'StaticController@sustentability');
        $router->get('/contacto', 'StaticController@contact');

        //Nosotros
        // $router->get($t->reverse('nosotros',$lang), 'StaticController@us');

    });

}

$router->post('/sendmail', 'StaticController@sendmail');
// Para administración
$router->group(['prefix' => 'admin','middleware' => 'auth'], function () use ($router) {
    $router->post('/save', 'AdminController@save');
});

$router->get('/login', 'LoginController@login');
$router->get('/logout', 'LoginController@logout');