@extends('layouts.default') 

@section('content') 

<style>
.admin-config {
    padding: 3em 0;
    min-height: 60vh;
}
.admin-config .title {
    color: #504037;
    font-size: 1.666em;
    text-transform: uppercase;
    letter-spacing: 0px;
    line-height: 1.35em;
    font-weight: 600;
}
.admin-config .title:after {
    content: ' ';
    display: block;
    width: 40px;
    height: 2px;
    margin: 0.14em 0;
    background-color: #b18857;
}
</style>

<section class="admin-config">

    <div class="container">
        
        <section>
            <div class="title">Carrusel</div>
            <div>
                
            </div>
        </section>
        <section>
            <div class="title">Intro</div>
        </section>
        <section>
            <div class="title">Welcome</div>
        </section>
        <section>
            <div class="title">Servicios</div>
        </section>
        <section>
            <div class="title">Productos</div>
        </section>
    </div>
</section>


@endsection