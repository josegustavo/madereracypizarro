@extends('layouts.default')

@section('title', 'Inicio')

@section('content') 

    @include('includes.header')

    @include('home.main_slide')
    @include('home.intro')
    @include('home.welcome')
    @include('home.what_we_do')
    @include('home.services')
    @include('home.products')
    @include('home.partners')

    
    @include('includes.footer')

@endsection