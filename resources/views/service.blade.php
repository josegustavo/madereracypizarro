@extends('layouts.default')
@section('title', ucfirst(mb_strtolower($service->title)) )
@push('styles')
    <style>
        .sidebar {
            padding: 6.1em 3.1em 5.2em;
            color: #8b857d;
            background-color: #e9e4d0;
            /*float: left;*/
            border: 2px solid;
            border-color: #b18857;
            margin-bottom: 3em;
            font-family: "Poppins", sans-serif;

        }
        .sidebar a {
            color: #504037;
        }
        .sidebar h5 {
            margin-top: 0;
            margin-bottom: 1.34em;
            font-size: 1.66em;
            line-height: 1.37em;
            font-weight: 600;
            text-transform: uppercase;
            color: #504037;
        }
        .sidebar li {
            position: relative;
            list-style: none;
            padding-left: 1em;
            font-size: 13px;
            line-height: 1.6em;
        }
        .sidebar li:before {
            content: '>';
            font-weight: bold;
            color: #b18857;
            font-size: 12px;
            position: absolute;
            left: 1px;
            top: 0;
        }
        .sidecontent {
            color: #8b857d;
            font-size: 13px;
            line-height: 1.6em;
            margin-bottom: 0.65em;
            position: relative;
            padding-bottom: 1em;
        }
        .sidecontent h5{
            color: #504037;
            padding-bottom: 5px;
            font-family: "Poppins", sans-serif;
            font-size: 1.66em;
            line-height: 1.37em;
            font-weight: 600;
            margin-bottom: 0.76em;
            text-transform: uppercase;
            letter-spacing: 0px;
            position: relative;
        }
        .sidecontent h5:after {
            content: ' ';
            display: block;
            position: absolute;
            border-top: 2px solid #222222;
            height: 0;
            width: 40px;
            min-width: 40px;
            left: 0;
            bottom: 0em;
            border-color: #b18857;
        }
        .sidecontent p {
            margin-bottom: 0.65em;
        }
        .post_info {
            color: #b18857;
            font-size: 1em;
            line-height: 1.2857em;
            font-weight: 500;
            margin-bottom: 2.1em;
            text-transform: uppercase;
        }
        .content-item {
            margin-bottom: 6em;
        }
        .image-container {
            color:#fff;
            margin-bottom: 0.3em;
            text-align: center;
        }
        .image-container img{
            max-width: 100%;
            min-height: 100px;
        }
    </style>
@endpush


@section('content')


    @include('includes.header')

    <div class="sub-header" style="background-image:url('{{url('/images/'.@$services->background)}}')" @editableimg(params,services,background)>
        <h1 @editable(services,$service_slug,title)>
            {{$service->title??'title'}}
        </h1>
        <ul class="breadcumb">
            <li>
                <a href="/">Inicio</a>
            </li>
            <li>
                <a href="/servicios">Servicios</a>
            </li>
        </ul>
    </div>
    <section class="home-section">
        <div class="container">
            <div class="row">
                <div class="four columns">
                    <div class="sidebar">
                        <h5>Services</h5>
                        <ul class="menu">
                            @foreach ($services_list?$services_list:[[]] as $key => $serv)
                            <li>
                                <a href="/servicios/{{$key}}">{{$serv->title??"title"}}</a>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="eight columns">
                    <div class="sidecontent" data-name="Bloque" @repeater(services,$service_slug,content_blocks,INDEX)>
                        @foreach($service->content_blocks??[[]] as $block_key => $block)
                        <div class="content-item" @repeateritem(services,$service_slug,content_blocks,$block_key)>
                            <div class="image-container" @editableimg(services,$service_slug,content_blocks,$block_key,image)>
                                <img src="{{url('/images/'.@$block->image)}}" />
                            </div>
                            <p class="post_info" @editable(services,$service_slug,content_blocks,$block_key,post_info)>
                                {{$block->post_info??''}}
                            </p>
                            <h5 class="title" @editable(services,$service_slug,content_blocks,$block_key,title)>
                                {{$block->title??'title'}}
                            </h5>
                            <div class="content" data-name="Parrafo" @repeater(services,$service_slug,content_blocks,$block_key,content,INDEX)>
                                @foreach((isset($block->content)&&$block->content)?$block->content:[''] as $content_key => $content)
                                    <p @repeateritem(services,$service_slug,content_blocks,$block_key,content,$content_key) >
                                        <span @editable(services,$service_slug,content_blocks,$block_key,content,$content_key)>{{$content?$content:'Contenido'}}</span>
                                    </p>
                                @endforeach
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>




    @include('includes.footer')

@endsection

