

@push('styles')
<style>
    .header_mobile,
    .navbar {
        display: none;
        background-color: #b18857;
    }

    /* Larger than tablet */

    @media (min-width: 750px) {
        /* Navbar */
        .navbar+.docs-section {
            border-top-width: 0;
        }
        .navbar,
        .navbar-spacer {
            display: block;
            width: 100%;
            height: 6.5rem;
            z-index: 99;
        }
        .navbar-spacer {
            display: none;
        }
        .navbar>.container {
            width: 100%;
        }
        .navbar-list {
            list-style: none;
            text-align: center;
            margin-bottom: 0;
        }
        .navbar-item {
            position: relative;
            display: inline-block;
            margin-bottom: 0;
        }
        .navbar-link {
            text-transform: uppercase;
            text-decoration: none;
            color: #f0eddf;
            padding: 1.8em 0.76em 1.65em;
            font-size: 1.166em;
            line-height: 6.5rem;
            font-weight: 500;
        }
        .navbar-link.active,
        .navbar-link:hover {
            color: rgba(80,64,55,0.8);
        }
        .has-docked-nav .navbar {
            position: fixed;
            top: 0;
            left: 0;
        }
        .has-docked-nav .navbar-spacer {
            display: block;
        }
        /* Re-overiding the width 100% declaration to match size of % based container */
        .has-docked-nav .navbar>.container {
            width: 80%;
        }
        
        .navbar-item:hover>.popover {
            opacity: 1;
            z-index: 1;
            pointer-events: all;
        }
        .popover:after {
            position: absolute;
            content: '';
            top: -13px;
            left: 0;
            width: 100%;
            height: 13px;
        }
        /* Popover */
        .popover.open {
            display: block;
        }
        .popover {
            display: block;
            transition: .2s ease-out;
            opacity: 0;
            z-index: -1;
            pointer-events: none;
            color: #f0eddf;
            position: absolute;
            top: 6.45em;
            background: #b18857;
            left: 50%;
            transform: translateX(-50%);
            -webkit-transform: translateX(-50%);
            line-height: 1.2857em;
            width: 255px;
            -webkit-filter: drop-shadow(0 0 6px rgba(0, 0, 0, .1));
            -moz-filter: drop-shadow(0 0 6px rgba(0, 0, 0, .1));
            filter: drop-shadow(0 0 6px rgba(0, 0, 0, .1));
        }
        .popover-item:first-child .popover-link:after,
        .popover-item:first-child .popover-link:before {
            bottom: 200%;
            left: 50%;
            border: solid transparent;
            content: " ";
            height: 0;
            width: 0;
            position: absolute;
            pointer-events: none;
        }
        .popover-item:first-child .popover-link:after {
            border-color: rgba(255, 255, 255, 0);
            border-bottom-color: #b18857;
            border-width: 10px;
            margin-left: -10px;
        }
        .popover-item:first-child .popover-link:before {
            border-color: rgba(238, 238, 238, 0);
            border-bottom-color: #b18857;
            border-width: 11px;
            margin-left: -11px;
        }
        .popover-list {
            padding: 0;
            margin: 0;
            list-style: none;
            padding: 2.45em 0; 
        }
        .popover-item {
            padding: 0;
            margin: 0;
        }
        .popover-link {
            position: relative;
            text-decoration: none;
            display: block;
            padding: 0 1.5em;
            text-align: center;
            line-height: 1.9em;
            font-size: 14px;
            color: #f0eddf;
        }
        .popover-item:first-child .popover-link {
            border-radius: 4px 4px 0 0;
        }
        .popover-item:last-child .popover-link {
            border-radius: 0 0 4px 4px;
            border-bottom-width: 0;
        }
        .popover-link.active,
        .popover-link:hover {
            color: #504037;
        }
    }
    .sub-header {
        padding: 6.3em 0;
        text-align: center;
        background-size: cover;
        position: relative;
    }

    h1 {
        color: #f0eddf;
        margin: 0.47em 0 0.47em;
        font-weight: 600;
        font-size: 4.5em;
        line-height: 1.2em;
        text-align: center;
        text-transform: none;
        letter-spacing: -1.06px;
    }
    .breadcumb {
        list-style: none;
        color: #f0eddf;
        display: inline-block;
        vertical-align: top;
        white-space: nowrap;
        font-size: 1.666em;
        font-weight: 400;
        text-align: center;
        text-transform: uppercase;
    }
    .breadcumb>li {
        display: inline-block;
    }
    .breadcumb>li:after {
        content:'/';
        margin-left: 0.3em;
    }
    .breadcumb>li:last-child:after {
        content: none;
    }
    .breadcumb a{
        color: #f0eddf;
    }
    .breadcumb>li.active a,
    .breadcumb>li.active {
        color: rgba(240,237,223,0.7)
    }

    .main-sub-title {
    color: #504037;
    font-size: 1.666em;
    text-transform: uppercase;
    letter-spacing: 0px;
    line-height: 1.35em;
    font-weight: 600;
    }
    .main-sub-title:after {
        content: ' ';
        display: block;
        width: 40px;
        height: 2px;
        margin: 0.14em 0;
        background-color: #b18857;
    }

    .highlight{
        position: relative;
    }
    .highlight>.background{
        position: absolute;
        height: 100%;
        width: 100%;
        overflow: hidden;
    }
    .highlight>.background>img{
        object-fit: cover;
        width: 100%;
        height: 100%;
    }
    .highlight>.content {
        position: relative;
        padding: 2.5em 1em;
        text-align: center;
        color: #e9e4d0;
        margin: 0 auto;
        font-size: 4.5em;
        line-height: 1.35em;
        font-weight: 600;
    }

    .block-dark {
        padding: 7em 4em;
        background-color: #b18857;
        color: #fff;
    }
    .block-dark>.title {
        font-weight: 600;
        margin-bottom: 0.5em;
        text-transform: none;
        line-height: 1.2em;
        font-size: 3.5em;
        letter-spacing: -1.06px;
    }
    .block-dark>.title:after {
        background-color: #fff;
        content: ' ';
        display: block;
        width: 40px;
        height: 2px;
        margin: 0.2em 0;
    }
    .block-dark>.content {
        font-size: 1.4em;
        font-style: normal;
        font-weight: 500;
        line-height: 1.35em;
        margin: 0.25em 0 2em 0;
    }

    @media (max-width: 749px) {
        .header_mobile {
            display: block;
        }
        .header_mobile .content_wrap {
            text-align: left;
            background-color: #b18857;
        }
        .header_mobile label {
            margin: 0;
        }
        .header_mobile .menu_button {
            left: auto;
            right: 30px;
            color: #FFF;
            fill: currentColor;
            width: 32px;
            height: 32px;
            float: right;
            padding: 8px;
            overflow: hidden;
        }
        .header_mobile .logo {
            background-image: url('/images/logo.png');
            padding-bottom: 48px;
            background-position: center;
            background-size: contain;
            background-repeat: no-repeat;
            margin: 0 auto;
            animation-duration: 0.5s;
            animation-timing-function: ease;
            transform: scale(1);
            margin-left: 2em;
            width: 100%;
            display: block;
            max-width: 100px;
        }
        .header_mobile .side_wrap {
            position: absolute;
            left: 0;
            right: 0;
            bottom: 0;
            width: 100%;
            top: 48px;
            background: #b18857;
            z-index: 99;
            overflow-y: auto;
            transition: all ease .3s;
            margin-left: 100%;
            max-width: 0;
        }
        .header_mobile input:checked~.side_wrap {
            margin-left: 0;
            max-width: 100%;
        }
        .header_mobile .menu_main_nav ul {
            margin: 0;
        }
        .header_mobile .menu_main_nav li {
            display: block;
            margin-bottom: 0;
            border-bottom: 1px solid rgba(0, 0, 0, 0.09);
        }
        .header_mobile .menu_main_nav > li a {
            color: #FFF;
            /*background-color: rgba(0,0,0,0.2);*/
            font-weight: bold;
            padding: 17px 35px;
            padding-right: 54px;
            font-size: 12px;
            line-height: 16px;
            display: block;
            position: relative;
        }
        .header_mobile input {
            display: none;
        }
        .header_mobile .open_child_menu {
            cursor: pointer;
            position: absolute;
            z-index: 1;
            right: .5em;
            top: .5em;
            font-size: 1.5em;
            text-align: center;
            width: 2em;
            height: 2.2857em;
            line-height: 2.2857em;
            -webkit-transition: all ease .3s;
            -moz-transition: all ease .3s;
            -ms-transition: all ease .3s;
            -o-transition: all ease .3s;
            transition: all ease .3s;
        }
        .header_mobile .menu-item-title {
            position: relative;
        }
        .header_mobile .sub-sub-menu{
            transition: all ease .5s;
            max-height: 0;
            overflow: hidden;
            opacity: 0;
        }
        .header_mobile input:checked~.sub-sub-menu {
            max-height: 800px;
            opacity: 1;
            background-color: rgba(0,0,0,0.1);
        }


        .highlight,
        .sub-header {
            font-size: 0.7em;
            padding: 2em 0;
        }
        .big-section {
            font-size: 0.7em;
            padding: 2em 2em!important;
        }

        .block-dark{
            padding: 3em 2em;
        }
    }
    
</style>
@endpush


<div class="navbar-spacer"></div>
<nav class="navbar">
    <div class="container">
        <ul class="navbar-list">
            @foreach($menus as $key => $item)
            <li class="navbar-item">
                <span>
                <a class="navbar-link" href="{{url($item->url)}}" @editable(params,menus,$key,name)>
                    {{__($item->name)}}
                </a></span>
                @if(array_key_exists('submenu',$item))
                <div class="popover">
                <ul class="popover-list">
                    @foreach($item->submenu as $sub_key => $sub_menu)
                    <li class="popover-item"><span class="popover-link">
                        <a href="{{url($sub_menu->url)}}" @editable(params,menus,$key,submenu,$sub_key,name)>
                            {{$sub_menu->name}}
                        </a></span>
                    </li>
                    @endforeach
                </ul>
                </div>
                @endif
            </li>
            @endforeach
        </ul>
    </div>
</nav>
<div class="header_mobile">
    <div class="content_wrap">
        <label class="menu_button icon-menu" for="menu-slide">
            <svg width="32" height="32" xmlns="http://www.w3.org/2000/svg"><path d="M4,10h24c1.104,0,2-0.896,2-2s-0.896-2-2-2H4C2.896,6,2,6.896,2,8S2.896,10,4,10z M28,14H4c-1.104,0-2,0.896-2,2  s0.896,2,2,2h24c1.104,0,2-0.896,2-2S29.104,14,28,14z M28,22H4c-1.104,0-2,0.896-2,2s0.896,2,2,2h24c1.104,0,2-0.896,2-2  S29.104,22,28,22z"/></svg>
        </label>
        <a href="/" class="logo logo_mobile"></a>
    </div>
    <input name="menu-slide" id="menu-slide" type="checkbox">
    <div class="side_wrap">
        <div class="panel_top">
            <nav class="menu_main_nav_area">
                <ul class="menu_main_nav">
                    @foreach($menus as $key => $item)
                        <li class="menu-item">
                            <div class="menu-item-title">
                                <a class="navbar-link" href="{{url($item->url)}}">
                                    <span>{{__($item->name)}}</span>
                                </a>
                                @if(array_key_exists('submenu',$item))
                                    <label class="open_child_menu" for="submenu-{{$key}}">&#x1f893;</label>
                                @endif
                            </div>
                            @if(array_key_exists('submenu',$item))
                                    <input type="radio" name="submenu-checker" id="submenu-{{$key}}">
                                    <ul class="sub-sub-menu">
                                        @foreach($item->submenu as $sub_key => $sub_menu)
                                            <li class="menu-item"><span>
                        <a href="{{url($sub_menu->url)}}">
                            {{$sub_menu->name}}
                        </a></span>
                                            </li>
                                        @endforeach
                                    </ul>

                            @endif
                        </li>
                    @endforeach
                </ul>

            </nav>
        </div>
        <div class="panel_bottom"></div>
    </div>
</div>