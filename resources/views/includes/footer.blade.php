
@push('styles')
<style>
    footer {
        background-color: #352f2a;
        padding: 6.35em 0 2em;
        font-size: 13px;
    }
    footer .logo {
        background-image: url(/images/logo.png);
        width: 100%;
        height: 60px;
        background-repeat: no-repeat;
        background-position: center;
        background-size: contain;
    }
    footer p {
        line-height: 1.6em;
        margin-top: 1.5em;
        color: #8b857d;
    }
    footer .copyright {
        padding: 3em 0 6.5em;
        color: #8b857d;
    }
    footer .content {
        margin-bottom: 3.2em;
    }
    .footer-title {
        color: #e9e4d0;
        margin-top: 1.15em;
        position: relative;
        margin-bottom: 1em;
        font-size: 1.66em;
        line-height: 1.37em;
        font-weight: 600;
        text-transform: uppercase;
    }
    .footer-title:after {
        background-color: #b18857;
        content: ' ';
        display: block;
        width: 35px;
        height: 2px;
        margin: 0.1em 0;
    }
    .footer-menu {
        list-style-type: none;
        margin-bottom: 0;
        padding-left: 0;
    }
    .footer-menu a{
        color: #8b857d
    }
    .footer-menu a:hover {
        color: #b18857;
    }
    .footer-menu,
    .footer-menu a {
        text-transform: capitalize;
    }
    footer .mini-map>iframe {
        max-width: 100%;
    }
    footer .columns {
        padding-left: 1.5em;
        padding-right: 1.5em;
    }

    @media (max-width: 749px) {
        footer .copyright {
            padding: 0;
            text-align: center;
        }
    }
</style>
@endpush

<footer>
    <div class="container">
        <div class="row content">
            <div class="four columns">
                <div class="logo"></div>
                <p class="logo-descr" @editable(params,us,who_we_are,content,0)>
                    {{$us->who_we_are->content[0]??'intro'}}
                </p>
                <div class="mini-map">
                <iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="{{$contact_us->iframe_map??'https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d868.6809925515792!2d-76.95368049239025!3d-12.163670106138522!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9105b8fb231a08e1%3A0x4f1629d127cddf35!2sMaderera%20SyC%20Pizarro!5e0!3m2!1ses-419!2spe!4v1571077956953!5m2!1ses-419!2spe'}}"
                    width="262" height="186" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
            <div class="four columns">
                <div class="footer-title" @editable(params,contact_us,title)>
                    {{$contact_us->title??'title'}}
                </div>
                <p>
                    Dirección:
                    <br> 
                    <span @editable(params,contact_us,address,0)>
                        {{@reset($contact_us->address)??'address'}}
                    </span>
                </p>
                <p>
                    Teléfono:
                    <br>
                    <span @editable(params,contact_us,phones,0)>
                        {{@reset($contact_us->phones)??'phone'}}
                    </span>
                </p>
                <p>
                    Dirección de correo:
                    <br>
                    <a href="mailto:{{@reset($contact_us->email)}}" @editable(params,contact_us,email,0)>
                        {{@reset($contact_us->email)??'email'}}
                    </a>
                </p>
            </div>
            <div class="four columns">
                @if (@count($contact_us->social) > 0)
                <div  class="footer-title">síguenos</div>
                <ul class="footer-menu">
                    @foreach($contact_us->social as $network => $url)
                    <li><a href="{{$url}}">{{$network}}</a></li>
                    @endforeach
                </ul>
                @endif
                <div>
                    <div class="footer-title">Menú</div>
                    <ul class="footer-menu">
                        @foreach($menus as $key => $item)
                        <li class="footer-menu-item">
                            <a href="{{url($item->url)}}" @editable(params,menus,$key,name)>
                                {{$item->name}}
                            </a>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        <div class="row copyright" @editable(params,us,copyright)>
            {{$us->copyright??'copyright'}}
        </div>
    </div>
    
</footer>
