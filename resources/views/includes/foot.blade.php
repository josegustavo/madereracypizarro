
@if(!app("auth")->guest())

<script>

    var save_value = function(name, value)
    {
        var request = new XMLHttpRequest();
        request.open('POST', '/admin/save', true);
        // request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        var data = new FormData();
        data.append('name', name);
        if(value !== null)
            data.append('value', value);
        // console.log(data);
        request.send(data);
        return request;
    }

    var onFocus = function(e){
        // console.log("focus",this.innerText);
        if(!this['initial-value'])
            this['initial-value'] = this.innerText;
    };

    var onBlur = function(e){
        // console.log("blur",this.textContent);
        var that = this;
        key = this.getAttribute('edit-key');
        value = this.innerText;
        if(this['initial-value'] != value)
        {
            var that = this;
            xhr = save_value(key, value);
            //Cambiar a los otros atributos que coincidan
            xhr.onreadystatechange = function(){
                if (xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200){
                    var all = document.querySelectorAll('[edit-key="'+key+'"]');
                    Array.prototype.forEach.call(all, function (el, i) {
                        if(el!=that) {
                            el.innerText = value;
                        }
                    });
                }
            };
            this['initial-value'] = value;
            var no_edit_url = this.getAttribute('no-url');
            if(!no_edit_url && this.tagName === 'A')
            {
                keys = this.getAttribute('edit-key').split('.');
                keys.splice(-1,1)
                keys.push('url');
                key = keys.join('.');
                var domain = window.location.protocol+'//'+window.location.hostname+'/';
                href = this.href.replace(domain,'/');
                text = "Ingrese una dirección url relativa para este enlace \""+that.innerText+"\" \n(sin incluir la base).\nEj. /nosotros/detalles#texto-2";
                var result = prompt(text, href);
                if(result){
                    xhr = save_value(key, result);
                    xhr.onreadystatechange = function(){
                        if (xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200){
                            var all = document.querySelectorAll('[edit-key="'+that.getAttribute('edit-key')+'"]');
                            Array.prototype.forEach.call(all, function (el, i) {
                                    el.setAttribute('href',result);
                            });
                        }
                    };
                }
            }
        }
        
    };
    
    function getRelativeCoordinates (container, e) {

        var pos = {}, offset = {}, ref;

        ref = container.offsetParent;

        pos.x = !! e.touches ? e.touches[ 0 ].pageX : e.pageX;
        pos.y = !! e.touches ? e.touches[ 0 ].pageY : e.pageY;

        offset.left = container.offsetLeft;
        offset.top = container.offsetTop;

        while ( ref ) {
            offset.left += ref.offsetLeft;
            offset.top += ref.offsetTop;
            ref = ref.offsetParent;
        }

        return { 
            x : pos.x - offset.left,
            y : pos.y - offset.top,
        }; 

    }

    var reSaveAll = function(element){
        var elements = element.querySelectorAll("[edit-key]");
        //console.log(elements)
        Array.prototype.forEach.call(elements, function(el, i){
            var key = el.getAttribute('edit-key');
            var value = el.innerText;
            save_value(key, value);
        });
        var images = element.querySelectorAll("[edit-image]");
        Array.prototype.forEach.call(images, function(el, i){
            var key = el.getAttribute('edit-image');
            var img = el.querySelector('img') || this;
            var value = null;
            if(img.getAttribute('src')){
                value = img.getAttribute('src')
            }else{
                value = img.style.backgroundImage
            }
            var domain = window.location.protocol+'//'+window.location.hostname+'/images/';
            value = value.replace(domain, '');
            save_value(key, value);
        });
    }

    var onClickImage = function(e){
        pos = getRelativeCoordinates(this,e);
        if(pos.x<(this.offsetWidth-50) || pos.y > 30) return;
        e.preventDefault();e.stopPropagation();
        var img = this.querySelector('img') || this;
        key = this.getAttribute('edit-image');
        var input = document.createElement("INPUT");
        input.setAttribute("type", "file");
        input.setAttribute("accept", "image/x-png,image/jpeg");
        input.addEventListener('change', function(e){
            xhr = save_value(key, input.files[0]);
            xhr.onreadystatechange = function(){
                if (xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200){
                    if(img.getAttribute('src')){
                        img.setAttribute('src', '/images/'+xhr.responseText);
                    }
                    else{
                        img.style.backgroundImage = 'url("/images/'+xhr.responseText+'")';
                    }
                    //console.log(img)                    
                }
            }
        })
        input.click();
    }

    var onClickRepeaterAdd = function(e){
        pos = getRelativeCoordinates(this,e);
        if(pos.x>100 || pos.y < this.offsetHeight) return;
        //console.log(pos.x,pos.y,this.offsetWidth,this.offsetHeight)
        e.preventDefault();e.stopPropagation();
        regex_text = this.getAttribute('repeater-add').replace(/\./g,'\\.').replace('INDEX','(\\d+)')
        
        var target = this.lastElementChild;
        if(!target) return;
        var wrap = document.createElement('div');
        wrap.appendChild(target.cloneNode(true));
        var last = wrap.innerHTML;
        
        var regex = new RegExp(regex_text, "g")
        var m = regex.exec(last)
        if(!m) return;
        var new_key = parseInt(m[1])+1;
        var search = m[0]
        var replace = this.getAttribute('repeater-add').replace('INDEX',new_key)
        
        var index = 0;
        do {
            last = last.replace(search, replace);
        } while((index = last.indexOf(search, index + 1)) > -1);
        
        this.insertAdjacentHTML('beforeend',last);
        activateEditor(this.lastElementChild);
        reSaveAll(this.lastElementChild)
    }

    var onClickRemoveItem = function(e){
        pos = getRelativeCoordinates(this,e);
        if(pos.x<(this.offsetWidth-18) || pos.y > 18) return;
        e.preventDefault();e.stopPropagation();
        var refresh = this.getAttribute('data-refresh');
        if(!refresh && this.parentNode.childElementCount<=1){
            alert("No puede eliminar el último elemento")
            return;
        }

        key = this.getAttribute('repeater-item')
        var that = this;
        xhr = save_value(key, null);
        //Cambiar a los otros atributos que coincidan
        xhr.onreadystatechange = function(){
            if (xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200){
                if(refresh)
                    location.reload()
                else
                    that.remove()
            }
        };
    }

    var onClickLink = function(e){
        var rect = e.target.getBoundingClientRect();
        if(e.clientX-rect.left>0) return;
        e.preventDefault();e.stopPropagation();
        window.location.href = this.href;
    }

    function insertTextAtCursor(text) {
        var sel, range, html;
        if (window.getSelection) {
            sel = window.getSelection();
            if (sel.getRangeAt && sel.rangeCount) {
            range = sel.getRangeAt(0);
            range.deleteContents();
            range.insertNode(document.createTextNode(text));
            }
        } else if (document.selection && document.selection.createRange) {
            document.selection.createRange().text = text;
        }
    }

    var activateEditor = function(element){
        var elements = element.querySelectorAll("[edit-key]");
        //console.log(elements)
        Array.prototype.forEach.call(elements, function(el, i){
            el.setAttribute('contenteditable', true);
            el.addEventListener('focus', onFocus);
            el.addEventListener('blur', onBlur);
            el.addEventListener("paste", function(e) {
                e.preventDefault();e.stopPropagation();
                if (e.clipboardData && e.clipboardData.getData) {
                    var text = e.clipboardData.getData("text/plain");
                    console.log(text)
                    document.execCommand("insertHTML", false, text);
                } else if (window.clipboardData && window.clipboardData.getData) {
                    var text = window.clipboardData.getData("Text");
                    insertTextAtCursor(text);
                }
            });
        });
        var images = element.querySelectorAll("[edit-image]");
        Array.prototype.forEach.call(images, function(el, i){
            el.addEventListener('click', onClickImage);
        });

        var lists = element.querySelectorAll("[repeater-add]");
        Array.prototype.forEach.call(lists, function(el, i){
            el.addEventListener('click', onClickRepeaterAdd);
        });

        var lists_items = element.querySelectorAll("[repeater-item]");
        Array.prototype.forEach.call(lists_items, function(el, i){
            el.addEventListener('click', onClickRemoveItem);
        });

        var links = element.querySelectorAll("a[edit-key]");
        Array.prototype.forEach.call(links, function(el, i){
            el.addEventListener('click', onClickLink);
        });
    };
    activateEditor(document);


    function home_add_banner(){
        var main_slides = document.querySelectorAll("input[name='main_slides']");
        var max_key = 0
        Array.prototype.forEach.call(main_slides, function(el, i){
            var value = el.getAttribute('value');
            if(value>max_key) max_key=value
        });
        max_key++;
        xhr = save_value('params.mainslider.'+max_key+'.new', true);
        xhr.onreadystatechange = function(){
            if (xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200){
                location.reload();
            }
        };
    }

    function home_delete_banner(){
        var selected_slide = document.querySelectorAll("input[name='main_slides']:checked")[0].value;
        xhr = save_value('params.mainslider.'+selected_slide, null);
        xhr.onreadystatechange = function(){
            if (xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200){
                location.reload();
            }
        };
    }
</script>

@endif