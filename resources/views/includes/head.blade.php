<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-149940156-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-149940156-1');
</script>

<link rel='dns-prefetch' href='//fonts.googleapis.com' />

<link rel="icon" type="image/vnd.microsoft.icon" href="/favicon.ico" sizes="32x32">

<link property="stylesheet" rel='stylesheet' id='tp-open-sans-css' href='https://fonts.googleapis.com/css?family=Open+Sans%3A300%2C400%2C600%2C700%2C800&#038;ver=4.9.7'
    type='text/css' media='all' />
<link property="stylesheet" rel='stylesheet' id='tp-raleway-css' href='https://fonts.googleapis.com/css?family=Raleway%3A100%2C200%2C300%2C400%2C500%2C600%2C700%2C800%2C900&#038;ver=4.9.7'
    type='text/css' media='all' />
<link property="stylesheet" rel='stylesheet' id='tp-droid-serif-css' href='https://fonts.googleapis.com/css?family=Droid+Serif%3A400%2C700&#038;ver=4.9.7'
    type='text/css' media='all' />

<link property="stylesheet" rel='stylesheet' id='fortunio-font-google_fonts-style-css' href='https://fonts.googleapis.com/css?family=Poppins:400,500,600,700|Sacramento&#038;subset=latin,latin-ext'
    type='text/css' media='all' />

<link rel="stylesheet" href="/css/normalize.css">
<link rel="stylesheet" href="/css/skeleton.css">

<style>
    body {
        font-family: "Poppins", sans-serif;
        font-size: 12px;
        line-height: 1.7em;
        font-weight: 400;
        text-rendering: optimizeLegibility;
        background-color: #e9e4d0;
        -webkit-font-smoothing: antialiased;
        -ms-word-wrap: break-word;
        word-wrap: break-word;
    }
    @keyframes show-top-zero {
        to {
            margin-top: 0;
            opacity: 1;
        }
    }

    .section-subtitle {
        color: #b18857;
        margin: 0 0 0.5em;
        text-transform: uppercase;
        text-align: center;
        position: relative;
        font-size: 1em;
        line-height: 1.25em;
        font-weight: 500;
    }

    .section-subtitle:before {
        content: ' ';
        background-image: url('data:image/svg+xml,%3Csvg xmlns="http://www.w3.org/2000/svg" viewBox="0 -150 1000 1000"%3E%3Cpath fill="rgba(177,136,87,0.28)" d="M906 653l49 9-73-151 38 7-97-201-98 201 38-7-29 61-49-101 42 8-108-223-100 206-35-72 51 10-130-268-131 268 52-10-58 118-96-198-99 203 39-7-74 153 49-9-76 158 127-24v90h718v-89l126 24zM251 544l-19-38 34 6zm302-73l-15 31-12-26zm106 172q-3 6-14 18-17 17-33 22-1 0-4-5 0-3-8.5-19.5T584 634l-14 15q-7 8-50 48 51 39 60 50-1-1 24 29 15 19 17 23 1 2 1 3v1l-3 4q-4 4-5 4.5t-12 4.5-12.5 3.5-5-4l-5-5L576 800l-4-12-11-17q-11-15-26-26l-39-28-38 28q-15 11-27 26-4 6-11 17-1 3-3 12l-3 10q-1 2-5 5.5t-6 4-13-3.5-12-5l-8-8 1-4q2-4 18-23 24-30 23-29 6-8 25-23-2 2 35-27-36-34-49-48l-14-15q-6 8-16 25-8 15-9.5 19.5T380 683q-12-4-23-13-9-8-17.5-18.5T333 643l-1-3q0-2 3.5-3.5T359 618l19-16-1-2 13-14q14-14 18-16l1 1 9-9q9 4 20 17.5t9 14-8 8.5l2.5 2.5 1.5 1.5q12 10 18 21 14 27 35 51 21-24 35-51 6-11 18-21l1.5-1.5 2.5-2.5q-6-8-8-8.5t9-14 20-17.5l9 9 1-1q4 2 18 16l13 14-2 2 19 16q21 17 24.5 18.5t4 2-.5 2.5z"/%3E%3C/svg%3E');
        position: absolute;
        top: -6em;
        left: 50%;
        transform: translateX(-50%);
        width: 5em;
        height: 5em;
        background-position: center;
        background-repeat: no-repeat;
        background-size: contain;

    }

    .section-title {
        color: #504037;
        text-align: center;
        margin-bottom: 1.25em;
        text-transform: uppercase;
        font-size: 3.5em;
        line-height: 1.2em;
        font-weight: 600;
    }

    section.home-section {
        padding-top: 10em;
        padding-bottom: 9em;
    }

    .image-icon-link:before {
        content: ' ';
        display: block;
        width: 4em;
        height: 4em;
        position: absolute;
        z-index: 10;
        left: 50%;
        top: 50%;
        transform: translateX(-50%) translateY(-50%) scale(0.3, 0.3);
        border-radius: 50%;
        transition: all .3s cubic-bezier(0.6, -0.28, 0.735, 0.045) 0s;
        opacity: 0;
        background-color: #fff;
        /* filter: alpha(opacity=0); */
        background-image: url('data:image/svg+xml,%3Csvg xmlns="http://www.w3.org/2000/svg" viewBox="-123 -279 1507 1500"%3E%3Cpath fill="#b18857" d="M540 669q14-14 34-14t36 14q32 34 0 70l-42 40q-56 56-133 56t-133-56-56-133 56-133l148-148q70-68 144-77t128 43q16 16 16 36t-16 36q-36 32-70 0-50-48-132 34L372 583q-26 26-26 64t26 63 63 25 63-26zM990 95q56 56 56 133t-56 133L832 519q-74 72-150 72-62 0-112-50-14-14-14-34t14-35 35-15 35 14q50 48 122-24l158-156q28-28 28-65t-28-61q-24-26-56-31t-60 21l-50 50q-16 14-36 14t-34-14q-34-34 0-70l50-50q54-54 127-51t129 61z"/%3E%3C/svg%3E');
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
    }
    .image-icon-link:hover:before {
        opacity: 1;
        /* filter: alpha(opacity=1); */
        transform: translateX(-50%) translateY(-50%) scale(1, 1);
    }


    @media (max-width: 768px) {
        section.home-section {
            padding-top: 6em;
            padding-bottom: 4.5em;
        }
    }

    section.light {
        background-color: #f0ece0;
        color: #8b857d;
    }
    section.dark {
        background-color: rgb(233, 228, 208);
    }
    @media (max-width: 639px) {
        .section-title {
            font-size: 1.666em;
        }
    }
</style>
@stack('styles')
<style>
    /*Editor*/    
    [edit-image]:after {
        content: 'Imagen';
        position: absolute;
        cursor: pointer;
        right: 0;
        top: 0;
        left:auto;
        bottom: auto;
        opacity: 1;
        text-align: center;
        padding: 3px 5px;   
        border: 2px solid rgb(233, 228, 208);
        z-index:98;
        background-color:#b18857;
        line-height: 1rem;
        font-size: 1rem;
    }
    [repeater-item] {
        position: relative;
    }
    [repeater-add]:before {
        content: "+ Agregar " attr(data-name);
        position: absolute;
        cursor: pointer;
        opacity: 1;
        top: 100%;
        text-align: center;
        padding: 3px 5px;
        border: 2px solid rgb(233, 228, 208);
        z-index: 98;
        background-color: #b18857;
        line-height: 1rem;
        font-size: 1rem;
        color: #fff;
        left: 0;
    }
    [repeater-item]::before {
        content: '\2718';
        position: absolute;
        cursor: pointer;
        right: -20px;
        opacity: 1;
        text-align: center;
        padding: 3px 5px;
        border: 2px solid rgb(233, 228, 208);
        z-index: 98;
        background-color: #b18857;
        line-height: 1rem;
        font-size: 1rem;
        color: #fff;
        top: 0;
    }
    a[edit-key] {
        position: relative;
    }
    a[edit-key]::after {
        content: '\2794';
        position: absolute;
        cursor: pointer;
        left: -16px;
        opacity: 1;
        text-align: left;
        padding: 3px;
        z-index: 98;
        background-color: #b18857;
        color: #fff;
        line-height: 1rem;
        font-size: 1rem;
        top: 0;
    }
    img.responsive {
        max-width: 100%;
    }
</style>

