@extends('layouts.default')

@section('title', ucfirst(mb_strtolower($product->title)) )

@push('styles')
<style>
    .products.home-section {
        background-color:#e9e4d0
    }
    section.products {
        background-color: rgba(233,228,208,1);
    }
    .product-item {
        background-color: #f0eddf;
    }
    .product-item-content {
        text-align: center;
        padding: 0 1em 1.5em;
        font-size: 13px;
        line-height: 1.6em;
    }
    .product-item-image {
        padding-bottom: 83.333%;
        display: block;
        position: relative;
        overflow: hidden;
        width: 100%;
        margin-bottom: 1.75em;
    }
    
    .product-item-image>img {
        position: absolute;
        height: 100%;
        object-fit: cover;
        width: 100%;
    }
    .product-item-title {
        font-size: 1em;
        font-weight: 400;
        line-height: 1.3em;
        margin-top: 0.8em;
        margin-bottom: 0.7em;
        padding: 0;
        max-height: 2.5em;
        text-transform: none;
        letter-spacing: 0;
        color: #504037;
    }
    
    .product-item-action {
        display: block;
        color: #fff;
        background-color: #504037;
        padding: 0.85em 2.58em 0.65em;
        font-weight: 500;
        text-transform: uppercase;
        margin-top: 1.25em;
        margin-bottom: 0;
    }
    .product-item-action:hover {
        background-color: #b18857;
        color: #fff;
    }


    .container.left {
        text-align: left;
    }
    .container.left .breadcumb {
        font-size: 1.266em;
    }
    .product-page.sub-header {
        padding: 6.3em 0 0 0;
    }
    .product-summary{
        margin-bottom: 3em;
    }
    .product-summary img{
        margin-bottom: 1em;
    }
    .product-summary .image-container {
        position: relative;
    }
    .product-summary .title {
        text-align: left;
        text-transform: uppercase;
        color: #504037;
        font-size: 3.5em;
        line-height: 1.3em;
        font-weight: 700;
        margin-bottom: 0.78em;
        margin-top: -7px;
    }
    .product-summary .subtitle{
        margin-bottom: 0.65em;
        font-weight: 600;
        font-size: 1.666em;
        color: #504037;
    }
    .product-summary .subtitle::after {
        display: block;
        width: 40px;
        height: 2px;
        content: '';
        margin-top: 7px;
        background-color: #b18857;
    }
    .product-summary .description {
        color: #8b857d;
        font-size: 13px;
        line-height: 1.6em;
        margin-bottom: 0.65em;
    }

    .product-page  h2 {
        text-align: left;
        margin-bottom: 0.7em;
        font-size: 2.2em;
        text-transform: uppercase;
        margin-top: 0.87em;
        color: #504037;
        font-family: "Poppins", sans-serif;
        line-height: 1.35em;
        font-weight: 600;
    }

    .product-attributes {
        position: relative;
        margin-bottom: 3em;
        padding-bottom: 1em;
    }
    .product-attribute {
        margin-bottom: 4em;        
    }
    .product-attribute .table{
        background-color: #f0eddf;
        color: #8b857d;
        font-size: 13px;
        line-height: 1.6em;
    }
    .product-attribute .table>.row{
        position: relative;
    }
    .product-attribute .columns {
        padding: 10px 15px;
    }

    .related-products>.row {
        margin-bottom: 3em;
    }
</style>
@endpush


@section('content') 


@include('includes.header')

<div class="product-page sub-header" style="background-image:url('{{url('/images/'.@$products->image)}}')">
    <div class="container left">
        <ul class="breadcumb">
            <li>
                <a href="/">Inicio</a>
            </li>
            <li>
                <a href="/productos">{{@$products->title}}</a>
            </li>
            <li class="active" @editable(products,$product_slug,title)>
                    {{$product->title}}
            </li>
        </ul>
    </div>
</div>
<section class="product-page products home-section">
    <div class="container">
        <div class="row  product-summary">
            <div class="five columns image-container"  @editableimg(products,$product_slug,image)>
                <img class="responsive" src="{{url('images/'.@$product->image)}}" />
            </div>
            <div class="seven columns">
                <h1 class="title" @editable(products,$product_slug,title)>
                    {{$product->title??"Title"}}
                </h1>
                <p class="subtitle" @editable(products,$product_slug,subtitle)>
                    {{@$product->subtitle}}
                </p>
                <div class="description">
                    @foreach ($product->description??[''] as $key => $desc)
                    <p @editable(products,$product_slug,description,$key)>
                        {{@$desc}}
                    </p>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="product-attributes" data-name="Tabla" @repeater(products,$product_slug,attributes,INDEX)>
            @foreach (@$product->attributes?$product->attributes:[[]] as $key => $attr)
            <div class="product-attribute" @repeateritem(products,$product_slug,attributes,$key)>
                <div class="title-container">
                    <h2 @editable(products,$product_slug,attributes,$key,name)>
                    {{@$attr->name}}
                    </h2>
                </div>
                <div class="table" data-name="Fila" @repeater(products,$product_slug,attributes,$key,table,INDEX)>
                    @foreach (@$attr->table?$attr->table:[[]] as $k_val => $val)
                    <div class="row" @repeateritem(products,$product_slug,attributes,$key,table,$k_val)>
                        <div class="six columns" @editable(products,$product_slug,attributes,$key,table,$k_val,key)>
                            {{@$val->key}}
                        </div>
                        <div class="six columns" @editable(products,$product_slug,attributes,$key,table,$k_val,val)>
                            {{@$val->val}}
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            @endforeach
        </div>
        <div class="related-products">
            <div>
                <h2>Related products</h2>
            </div>
            <div class="row">
                @foreach (@$products_list as $key => $item)
                    @break($loop->iteration>8)
                    <div class="three columns">
                        <div class="product-item">
                            <a class="product-item-image image-icon-link" href="{{@$item->action->url}}">
                                <img src="{{url('images/'.@$item->image_texture)}}"/>
                            </a>
                            <div class="product-item-content">
                                <div class="product-item-title">
                                    {{$item->title??''}}
                                </div>
                                <a class="product-item-action" href="{{@$item->action->url}}">
                                    {{$item->action->text??'Ver'}}
                                </a>
                            </div>
                        </div>
                    </div>
            @if(($loop->index+1) % 4 === 0)
            </div>
            <div class="row">
            @endif
                @endforeach
            </div>
        </div>
    </div>
</section>

@include('includes.footer')

@endsection

