<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Maderera S&C Pizarro - @yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge"> 
    @include('includes.head')
    
</head>

<body class="has-docked-nav">
    
    <div class="docs-section">
            @yield('content') 
    </div>
    @include('includes.foot')
</body>

</html>