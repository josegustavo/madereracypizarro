@push('styles')
<style>
    .mainslider {
        position: relative;
        width: 100%;
        /* padding-bottom: calc(100vmin - 6.5rem - 1px); */
        padding-bottom: 46.3%;
        overflow: hidden;
    }
    .mainslider>input+div {
        position: absolute;
        width: 100%;
        height: 100%;
        opacity: 0;
        z-index: -1;
        transition: all 0.5s cubic-bezier(0.4, 1.3, 0.65, 1);
        background-position: center;
        background-size: cover;
        background-repeat: no-repeat;
    }
    .mainslider>input:checked+div {
        opacity: 1;
        z-index: 1;
    }
    .mainslider>input {
        display: none;
    }
    .mainslider>.navigation {
        position: absolute;
        bottom: 3vmax;
        left: 50%;
        z-index: 10;
        font-size: 0;
        line-height: 0;
        text-align: center;
        -webkit-touch-callout: none;
        -webkit-user-select: none;
        -khtml-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }
    .mainslider>.navigation>div {
        margin-left: -100%;
    }
    .mainslider>.navigation label {
        background-color: rgba(240, 237, 223, 0.4);
        border: 2px solid transparent;
        border-radius: 50%;
        -webkit-border-radius: 50%;
        box-sizing: border-box;
        background: rgba(125, 125, 125, 0.5);
        cursor: pointer;
        position: relative;
        display: inline-block;
        margin: 0 4px;
        padding: 5px;
    }
    .mainslider>.navigation label:hover,
    .mainslider>input:nth-of-type(1):checked~.navigation label:nth-of-type(1),
    .mainslider>input:nth-of-type(2):checked~.navigation label:nth-of-type(2),
    .mainslider>input:nth-of-type(3):checked~.navigation label:nth-of-type(3),
    .mainslider>input:nth-of-type(4):checked~.navigation label:nth-of-type(4),
    .mainslider>input:nth-of-type(5):checked~.navigation label:nth-of-type(5),
    .mainslider>input:nth-of-type(6):checked~.navigation label:nth-of-type(6),
    .mainslider>input:nth-of-type(7):checked~.navigation label:nth-of-type(7),
    .mainslider>input:nth-of-type(8):checked~.navigation label:nth-of-type(8),
    .mainslider>input:nth-of-type(9):checked~.navigation label:nth-of-type(9),
    .mainslider>input:nth-of-type(10):checked~.navigation label:nth-of-type(10),
    .mainslider>input:nth-of-type(11):checked~.navigation label:nth-of-type(11) {
        border-color: #f0eddf;
        background-color: rgba(240, 237, 223, 0)
    }
    @keyframes scale-bg-up {
        0% {
            transform: scale(1);
        }
        50% { 
            transform: scale(1.2);
        }
        100% {
            transform: scale(1);
        }
    }
    @keyframes scale-bg-down {
        0% {
            transform: scale(1.2);
        }
        50% { 
            transform: scale(1);
        }
        100% {
            transform: scale(1.2);
        }
    }
    .mainslider>input:nth-of-type(1):checked+div>.mainslider-bg>img,
    .mainslider>input:nth-of-type(3):checked+div>.mainslider-bg>img,
    .mainslider>input:nth-of-type(5):checked+div>.mainslider-bg>img,
    .mainslider>input:nth-of-type(7):checked+div>.mainslider-bg>img,
    .mainslider>input:nth-of-type(9):checked+div>.mainslider-bg>img {
        animation-name: scale-bg-up; 
        
    }
    .mainslider>input:nth-of-type(2):checked+div>.mainslider-bg>img,
    .mainslider>input:nth-of-type(4):checked+div>.mainslider-bg>img,
    .mainslider>input:nth-of-type(6):checked+div>.mainslider-bg>img,
    .mainslider>input:nth-of-type(8):checked+div>.mainslider-bg>img,
    .mainslider>input:nth-of-type(10):checked+div>.mainslider-bg>img {
        animation-name: scale-bg-down; 
    }
    .mainslider-content {
        text-align: center;
        margin: 0 auto;
        color: rgba(233, 228, 208, 1.00);
        text-transform: uppercase;
        height: 100%;
        padding-top: 1em;
    }
    @keyframes scale-logo {
        from {
            transform: scale(0);
        }
        to { 
            transform: scale(1);
        }
    }
    .mainslider-content>.logo {
        background-image: url('/images/logo.png');
        width: 15%;
        max-width: 240px;
        padding-bottom: 15%;
        background-position: center;
        background-size: contain;
        background-repeat: no-repeat;
        margin: 0 auto;
        animation-duration: 0.5s;
        animation-timing-function: ease;
        transform: scale(1);
    }
    .mainslider>input:checked+div>.mainslider-content>.logo {
        animation-name: scale-logo; 
    }
    .mainslider-content>.intro {
        font-weight: 400;
        line-height: 3em;
        margin-top: -10em;
        /* transition: .4s .4s; */
        opacity: 0;
        line-height: 1.1em;
        font-size: 1.5em;
    }
    .mainslider-content>.intro:after {
        content: '';
        display: block;
        margin: 0 auto;
        width: 40px;
        height: 2px;
        background-color: #b18857;
    }
    .mainslider>input:checked+div>.mainslider-content>.intro {
        animation: show-top-zero .5s ease 0.5s forwards;
    }
    .mainslider-content>.title {
        letter-spacing: 0px;
        font-weight: 700;
        font-size: 7.5vmax;
        line-height: 1.5em;
        white-space: nowrap;
        margin-top: -1em;
        opacity: 0;
    }
    .mainslider>input:checked+div>.mainslider-content>.title {
        animation: show-top-zero .5s ease 1s forwards;
    }
    .mainslider-content>.action {
        text-decoration: none;
        padding: 7.5px 22px;
        font-weight: 500;
        line-height: 2em;
        background-color: rgba(177, 136, 87, 1.00);
        width: 100%;
        max-width: 150px;
        margin: 1vmax auto;
        display: block;
        color: rgba(233, 228, 208, 1.00);
        margin-top: 10em;
        opacity: 0;
    }
    .mainslider-content>.action:hover {
        color: rgb(80, 64, 55);
        background-color: rgb(233, 228, 208);
    }
    .mainslider>input:checked+div>.mainslider-content>.action {
        animation: show-top-zero .5s ease 1.5s forwards;
    }
    .mainslider-bg {
        display: block;
        position: absolute;
        width: 100%;
        height: 100%;
        /* z-index: -1; */
        /* filter: brightness(50%); */        
    }
    .mainslider-bg>img {
        height: 100%;
        object-fit: cover;
        width: 100%;
        animation-duration: 40s; 
        animation-iteration-count: infinite;
        animation-timing-function: ease;
    }
    sub.actionjs {
        position: absolute;
        color:#fff;
        padding: 0.5em;
        line-height: 12px;
        cursor: pointer;
        right: 0;
        background-color: #b18857;
        z-index: 2;
        border: 0px solid rgb(233, 228, 208);
    }
    sub.add-banner{
        bottom: 20px;
    }
    @media (max-width: 639px) {
        .mainslider {
            padding-bottom: 85vmin;
        }
        .mainslider-content>.intro {
            font-size: 1em;
            padding: 1em 1em;
            line-height: 1.5em;
        }
        .mainslider-content>.title {
            padding: 0.6em 0;
        }
        .mainslider-content>.logo {
            width: 100%;
        }
        .mainslider-content>.action {
            font-size: 0.8em;
        }
    }
    @media (max-width: 749px) {
        .mainslider-content>.logo {
            display: none;
        }
    }
</style>
@endpush

<section>
    <div class="mainslider">

        @auth
            <sub class="actionjs add-banner" onclick="home_add_banner()">Agregar banner</sub>
            <sub class="actionjs delete-banner" onclick="home_delete_banner()">Eliminar banner</sub>
        @endauth
        @foreach (@$mainslider?$mainslider:[[]] as $key => $slid)
        <input type="radio" name="main_slides" id="mainslid_{{$loop->iteration}}" {{$loop->first?'checked':''}} value="{{$key}}"/>
        <div>
            <div class="mainslider-bg" @editableimg(params,mainslider,$key,image)>
                <img src="{{url('images/'.($slid->image??''))}}" />
            </div>
            <div class="mainslider-content">
                <div class="logo"></div>
                <div class="intro" @editable(params,mainslider,$key,sub_title)>
                    {{$slid->sub_title??'Sub título'}}
                </div>
                <div class="title" @editable(params,mainslider,$key,title)>
                    {{$slid->title??'Título'}}
                </div>
                <a class="action" href="{{$slid->action->url??url()}}" @editable(params,mainslider,$key,action,name)>
                    {{$slid->action->name??__('Más información')}}
                </a>
            </div>
        </div>
        @endforeach
        <div class="navigation">
            <div>
                @foreach ($mainslider as $key => $slid)
                <label for="mainslid_{{$loop->iteration}}"></label>
                @endforeach
            </div>
        </div>
    </div>
</section>