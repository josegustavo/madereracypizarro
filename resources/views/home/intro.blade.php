
@push('styles')
<style>
    section.intro {
        padding-top: 4.5em;
        background-color: #b18857;
    }
    section.intro .row {
        margin-top: 0;
        opacity: 0;
        animation: show-top-zero .5s ease 0.25s forwards;
    }
    .intro-item {
        text-align: center;
    }
    .intro-item-icon {
        display: block;
        position: relative;
        background-color: #f0eddf;
        margin: 0 auto;
        width: 7.5em;
        height: 7.5em;
        line-height: 7.5em;
        border-radius: 50%;
        background-position: center;
        background-size: cover;
    }
    .intro-item-icon:hover {
        background-color: #504037;
    }
    .intro-item-title{
        display: block;
        font-size: 1.666em;
        line-height: 1.35em;
        text-transform: uppercase;
        letter-spacing: 0px;
        margin-top: 0.75em;
        font-weight: 600;
    }
    .intro-item-title:after {
        content: '';
        width: 40px;
        height: 2px;
        margin: 0 auto;
        display: block;
        margin-top: 5px;
        background-color: #f0eddf;
    }
    .intro-item-desc {
        margin-top: 1.1em;
        overflow: hidden;
        margin-bottom: 0.5em;
        word-wrap: break-word;
        font-size: 13px;
        line-height: 1.6em;
        max-width: 250px;
        margin-left: auto;
        margin-right: auto;
    }
    @media (max-width: 639px) {
        .intro-item-title{
            font-size: 1.2em;
        }
    }
</style>
@endpush

<section class="intro">
    <div class="container">
        <div class="row">
            @foreach ($intros as $key => $intro)
            <div class="{{(count($intros)==3)?'four':'three'}} columns">
                <div class="intro-item">
                    <a class="intro-item-icon" href="{{$intro->url??''}}" style="background-image: url('{{url('images/'.@$intro->icon)}}')" @editableimg(params,'intros',$key,'icon')>
                    </a>
                    <div class="intro-item-content">
                        <span class="intro-item-title h4">
                            <a href="{{@$intro->url}}" @editable(params,'intros',$key,'title')>{{$intro->title??'title'}}</a>
                        </span>
                        <div class="intro-item-desc">
                            <p @editable(params,'intros',$key,'description')>{{$intro->description??'description'}}</p>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>


