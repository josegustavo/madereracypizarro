@push('styles')
<style>
    section.what-we-do {
        background-color: #f0ece0;
    }
    .wwd-item-content {
        text-align: center;
        padding: 0 1em 1.5em;
        font-size: 13px;
        line-height: 1.6em;
    }
    .wwd-item-image {
        padding-bottom: 67.5%;
        display: block;
        position: relative;
        overflow: hidden;
        width: 100%;
        margin-bottom: 1.75em;
    }
    .wwd-item-image:after {
        content: ' ';
        position: absolute;
        z-index: 1;
        top: 0;
        left: 0;
        bottom: 0;
        right: 0;
        opacity: 0;
        background-color: rgba(177,136,87,0.6);
        transition: opacity .3s ease;
    }
    .wwd-item-image:hover:after {
        opacity: 1;
    }
    
    .wwd-item-image>img {
        position: absolute;
        height: 100%;
        object-fit: cover;
        width: 100%;
    }
    .wwd-item-title {
        margin: 1em 0 0;
        font-size: 1.666em;
        text-transform: uppercase;
        letter-spacing: 0px;
        line-height: 1.35em;
        font-weight: 600;
    }
    .wwd-item-title:after {
        background-color: #b18857;
        content: '';
        width: 40px;
        height: 2px;
        margin: 0 auto;
        display: block;
        margin-top: 5px;
    }
    .wwd-item-title>a {
        color: #504037;
    }
    .wwd-item-title>a:hover {
        color: #7f6342;
    }
    .wwd-item-desc {
        color: #8b857d;
        margin-top: 1.1em;
        overflow: hidden;
        margin-bottom: 0.5em;
    }
</style>
@endpush

<section class="home-section what-we-do">
    <div class="container">
        <div>
            <div class="section-subtitle" @editable(params,'what_we_do','intro')>
                {{$what_we_do->intro??'ABOUT US'}}
            </div>
            <h3 class="section-title" @editable(params,'what_we_do','title')>
                {{$what_we_do->title??'WHAT WE DO!'}}
            </h3>
        </div>
        <div class="row">
            @foreach ($what_we_do->slides??[[],[],[]] as $key => $item)
            <div class="four columns">
                <div class="wwd-item">
                    <a class="wwd-item-image image-icon-link" href="{{@$item->url}}" @editableimg(params,'what_we_do','slides',$key,'image')>
                        <img src="{{url('images/'.@$item->image)}}"/>
                    </a>
                    <div class="wwd-item-content">
                        <span class="wwd-item-title h4">
                            <a href="{{@$item->url}}" @editable(params,'what_we_do','slides',$key,'title')>
                                {{$item->title??'Title'}}
                            </a>
                        </span>
                        <div class="wwd-item-desc" >
                            <p @editable(params,'what_we_do','slides',$key,'description')>{{$item->description??'Description'}}</p>
                        </div>
                    </div>
                </div>
            </div>
            @break($loop->iteration == 3)
            @endforeach
        </div>
        <div>
        </div>
    </div>
</section>