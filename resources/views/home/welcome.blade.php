
@push('styles')

<style>
    section.welcome {
        background-color: rgb(233, 228, 208);
    }
    .slidual {
        position: relative;
        max-width: 100%;
        padding-bottom: 46%;
    }

    .slidual>input+div {
        position: absolute;
        display: inline-block;
        width: 100%;
        height: 100%;
        opacity: 0;
        z-index: -1;
        transition: all 0.5s cubic-bezier(0.4, 1.3, 0.65, 1);
        background-position: center;
        background-size: cover;
        background-repeat: no-repeat;
    }

    .slidual>input:checked+div {
        opacity: 1;
        z-index: 1;
    }

    .slidual>input {
        display: none;
    }

    .slidual>input:nth-of-type(10):checked~ul li:first-of-type {
        margin-left: -900%;
    }

    .slidual>input:nth-of-type(9):checked~ul li:first-of-type {
        margin-left: -800%;
    }

    .slidual>input:nth-of-type(8):checked~ul li:first-of-type {
        margin-left: -700%;
    }

    .slidual>input:nth-of-type(7):checked~ul li:first-of-type {
        margin-left: -600%;
    }

    .slidual>input:nth-of-type(6):checked~ul li:first-of-type {
        margin-left: -500%;
    }

    .slidual>input:nth-of-type(5):checked~ul li:first-of-type {
        margin-left: -400%;
    }

    .slidual>input:nth-of-type(4):checked~ul li:first-of-type {
        margin-left: -300%;
    }

    .slidual>input:nth-of-type(3):checked~ul li:first-of-type {
        margin-left: -200%;
    }

    .slidual>input:nth-of-type(2):checked~ul li:first-of-type {
        margin-left: -100%;
    }

    .slidual>input:nth-of-type(1):checked~ul li:first-of-type {
        margin-left: 0%;
    }

    .slidual>ul {
        display: inline-block;
        position: absolute;
        width: 50%;
        z-index: 1;
        font-size: 0;
        line-height: 0;
        margin: 0 auto;
        white-space: nowrap;
        -moz-box-sizing: border-box;
        -webkit-box-sizing: border-box;
        box-sizing: border-box;
        overflow: hidden;
        height: 100%;
    }

    .slidual-right {
        background-color: rgb(80, 64, 55);
        left: 50%;
    }
    .slidual-content{
        padding: 6.5em 2em 4em 4em;
        white-space: initial;
        font-size: 12px;
    }

    .slidual>ul>li {
        position: relative;
        display: inline-block;
        width: 100%;
        height: 100%;
        overflow: hidden;
        -moz-transition: all 0.5s cubic-bezier(0.4, 1.3, 0.65, 1);
        -o-transition: all 0.5s ease-out;
        -webkit-transition: all 0.5s cubic-bezier(0.4, 1.3, 0.65, 1);
        transition: all 0.5s cubic-bezier(0.4, 1.3, 0.65, 1);
        margin-bottom: 0;
    }

    .slidual>.arrows {
        -webkit-touch-callout: none;
        -webkit-user-select: none;
        -khtml-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    .slidual .arrows label {
        border-left-color: #B1B1B1;
        border-right-color: #B1B1B1;
    }

    .slidual-content>.title {
        font-size: 3.5em;
        color: #e9e4d0;
        line-height: 1.35em;
        font-weight: 600;
        margin-top: 0.54em;
        margin-bottom: 0.54em;
        letter-spacing: -1.06px;
    }

    .slidual-content>.title:after {
        content: ' ';
        display: block;
        width: 40px;
        height: 2px;
        margin: 0.18em 0;
        background-color: #b18857;
    }

    .slidual-content>.desc {
        font-weight: 400;
        font-style: normal;
        margin-bottom: 0;
        margin-top: 1.1em;
        line-height: 1.3em;
        text-align: left;
        font-size: 1.5em;
    }

    .slidual-content>.action {
        color: #504037;
        background-color: #f0eddf;
        font-size: 1.166em;
        padding: 1em 3.3em;
        margin-top: 2em;
        line-height: 1.2857em;
        display: inline-block;
        text-align: center;
        font-weight: 500;
        text-transform: uppercase;
    }

    .slidual-content>.action:hover {
        color: #f0eddf;
        background-color: #b18857;
    }

    .slidual-bg {
        position: absolute;
        width: 100%;
        height: 100%;
        z-index: -1;
    }
    
    .slidual-left img {
        height: 100%;
        object-fit: cover;
        width: 100%;
        min-height: 250px;
    }

    .slidual>.arrows {
        position: absolute;
        left: -40px;
        top: 0;
        width: 40px;
        height: 80px;
        z-index: 0;
        -moz-box-sizing: content-box;
        -webkit-box-sizing: content-box;
        box-sizing: content-box;
        /* background-color: #b18857; */
    }

    .slidual>.arrows label {
        position: absolute;
        cursor: pointer;
        left: 0;
        background-color: #b18857;
        height: 40px;
        width: 40px;
        display: none;
    }

    .slidual>.arrows label:hover {
        background-color: #504037;
    }

    .slidual>.arrows label:after {
        content: '';
        position: absolute;
        border-left: 2px solid #fff;
        border-top: 2px solid #fff;
        transform: rotate(135deg);
        width: 8px;
        height: 8px;
        padding: 2px;
        top: 28%;
        left: 26%;
    }

    .slidual>input:first-of-type:checked~.arrows label:last-of-type,
    .slidual>input:nth-of-type(1):checked~.arrows label:nth-of-type(0),
    .slidual>input:nth-of-type(2):checked~.arrows label:nth-of-type(1),
    .slidual>input:nth-of-type(3):checked~.arrows label:nth-of-type(2),
    .slidual>input:nth-of-type(4):checked~.arrows label:nth-of-type(3),
    .slidual>input:nth-of-type(5):checked~.arrows label:nth-of-type(4),
    .slidual>input:nth-of-type(6):checked~.arrows label:nth-of-type(5),
    .slidual>input:nth-of-type(7):checked~.arrows label:nth-of-type(6),
    .slidual>input:nth-of-type(8):checked~.arrows label:nth-of-type(7),
    .slidual>input:nth-of-type(9):checked~.arrows label:nth-of-type(8),
    .slidual>input:nth-of-type(10):checked~.arrows label:nth-of-type(9),
    .slidual>input:nth-of-type(11):checked~.arrows label:nth-of-type(10) {
        display: block;
        left: 0;
        transform: rotate(180deg);
    }

    .slidual>input:last-of-type:checked~.arrows label:first-of-type,
    .slidual>input:nth-of-type(1):checked~.arrows label:nth-of-type(2),
    .slidual>input:nth-of-type(2):checked~.arrows label:nth-of-type(3),
    .slidual>input:nth-of-type(3):checked~.arrows label:nth-of-type(4),
    .slidual>input:nth-of-type(4):checked~.arrows label:nth-of-type(5),
    .slidual>input:nth-of-type(5):checked~.arrows label:nth-of-type(6),
    .slidual>input:nth-of-type(6):checked~.arrows label:nth-of-type(7),
    .slidual>input:nth-of-type(7):checked~.arrows label:nth-of-type(8),
    .slidual>input:nth-of-type(8):checked~.arrows label:nth-of-type(9),
    .slidual>input:nth-of-type(9):checked~.arrows label:nth-of-type(10),
    .slidual>input:nth-of-type(10):checked~.arrows label:nth-of-type(11),
    .slidual>input:nth-of-type(11):checked~.arrows label:nth-of-type(12) {
        display: block;
        right: 0;
        /* transform: rotate(225deg); */
        top: 40px;
    }

    @media (max-width: 768px) {
        .slidual {
            padding-bottom: 0;
        }
        .slidual>ul {
            width: 100%;
            display: block;
            position: initial;
        }
        .slidual>.arrows {
            left: calc(100% - 40px);
        }
    }
    
    @media (max-width: 1024px) {
        .slidual-content {
            padding: 1em;
        }
    }
    @media (max-width: 639px) {
        .slidual-content>.title {
            font-size: 1.5em;
        }
        .slidual-content>.desc {
            font-size: 13px;
        }
        .slidual-content>.action {
            display: none;
        }
    }
</style>
@endpush


<section class="home-section welcome">
    <div class="container">
        <div>
            
            <div class="section-subtitle"  @editable(params,welcome,intro)>{{$welcome->intro??"Here we are"}}</div>
            <h3 class="section-title"  @editable(params,welcome,title)>{{$welcome->title??"Welcome"}}</h3>
        </div>
        <div class="slidual">
            @foreach ($welcome->slides as $key => $prod)
            <input type="radio" name="slidual" id="slidual_{{$loop->iteration}}" {{$loop->first?'checked':''}} /> 
            @endforeach
            <ul class="slidual-left">
                @foreach ($welcome->slides as $key => $prod)
                <li @editableimg(params,welcome,slides,$key,image)>
                    <img src="{{url('images/'.@$prod->image)}}" />
                </li>
                @endforeach
            </ul>
            <ul class="slidual-right">
                @foreach ($welcome->slides as $key => $prod)
                <li>
                    <div class="slidual-content">
                        <div class="title" @editable(params,welcome,slides,$key,title)>
                                {{$prod->title??"Title"}}
                        </div>
                        <div class="desc" @editable(params,welcome,slides,$key,description)>
                            {{$prod->description??"Description"}}
                        </div>
                        <a class="action" href="{{@$prod->url}}" @editable(params,welcome,slides,$key,action_text)>
                             {{$prod->action_text??"action"}}
                        </a>
                    </div>
                </li>
                @endforeach
            </ul>
            <div class="arrows">
                @foreach ($welcome->slides as $key => $prod)
                <label for="slidual_{{$loop->iteration}}"></label>
                @endforeach
            </div>
        </div>
    </div>
</section>