
@push('styles')
<style>
    section.products {
        background-color: rgba(233,228,208,1);
    }
    .product-item {
        background-color: #f0eddf;
        margin-bottom: 3em;
    }
    .product-item-content {
        text-align: center;
        padding: 0 1em 1.5em;
        font-size: 13px;
        line-height: 1.6em;
    }
    .product-item-image {
        padding-bottom: 83.333%;
        display: block;
        position: relative;
        overflow: hidden;
        width: 100%;
        margin-bottom: 1.75em;
    }
    
    .product-item-image>img {
        position: absolute;
        height: 100%;
        object-fit: cover;
        width: 100%;
    }
    .product-item-title {
        font-size: 1em;
        font-weight: 400;
        line-height: 1.3em;
        margin-top: 0.8em;
        margin-bottom: 0.7em;
        padding: 0;
        max-height: 2.5em;
        text-transform: none;
        letter-spacing: 0;
        color: #504037;
    }
    
    .product-item-action {
        display: block;
        color: #fff;
        background-color: #504037;
        padding: 0.85em 2.58em 0.65em;
        font-weight: 500;
        text-transform: uppercase;
        margin-top: 1.25em;
        margin-bottom: 0;
    }
    .product-item-action:hover {
        background-color: #b18857;
        color: #fff;
    }

    .action-main {
        text-align: center
    }
    .btn-main {
        margin-top: 2em;
        min-width: 90px;
        color: #f0eddf;
        background-color: #b18857;
        font-size: 1.166em;
        padding: 1em 3.3em;
        line-height: 1.2857em;
        display: inline-block;
        text-align: center;
        font-weight: 500;
        text-transform: uppercase;
    }
    .btn-main:hover {
        color: #f0eddf;
        background-color: #504037;
    }
</style>
@endpush

<section class="home-section products">
    <div class="container">
        <div>
            <div class="section-subtitle" @editable(params,products_home,intro)>
                {{$products_home->intro??'intro'}}
            </div>
            <h3 class="section-title" @editable(params,products_home,title)>
                 {{$products_home->title??'title'}}
            </h3>
        </div>
        <div class="row">
            @foreach ($products_home->maderas??[[],[],[],[]] as $key => $item)
            <div class="three columns">
                <div class="product-item">
                    <a class="product-item-image image-icon-link" href="{{@$item->url}}" @editableimg(params,products_home,maderas,$key,image')>
                        <img src="{{url('images/'.@$item->image)}}"/>
                    </a>
                    <div class="product-item-content">
                        <div class="product-item-title" @editable(params,products_home,maderas,$key,name)>
                            {{$item->name??'name'}}
                        </div>
                        <a class="product-item-action" href="{{@$item->action->url}}" @editable(params,products_home,maderas,$key,action,text)>
                            {{$item->action->text??'action'}}
                        </a>
                    </div>
                </div>
            </div>
            @break($loop->iteration >= 4)
            @endforeach
        </div>
        <div class="row action-main">
            <a href="{{@$products_home->home_action->url}}" class="btn-main" @editable(params,products_home,home_action,text)>
                {{$products_home->home_action->text??'title'}}
            </a>
        </div>
    </div>
</section>