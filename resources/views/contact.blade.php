@extends('layouts.default')
@section('title', ucfirst(mb_strtolower($contact_us->title)) )
@push('styles')
    <style>

    .form-contact input,
    .form-contact textarea{
        width: 100%;
        max-width: 100%;
        background-color: #f0eddf;
        border-color: rgba(221,221,221,0);
        color: #8a8a8a;
        font-size: 1em;
        text-align: left;
        line-height: 2.1em;
        padding: 2em 1.5em 2em 2.4em;
        vertical-align: baseline;
        border-radius: 0;
        -webkit-transition: all ease .3s;
        -moz-transition: all ease .3s;
        -ms-transition: all ease .3s;
        -o-transition: all ease .3s;
        transition: all ease .3s;
    }

    .form-contact input:focus,
    .form-contact textarea:focus{
        border-color: #504037;
    }

    .form-contact input:hover,
    .form-contact textarea:hover{
        border-color: #b18857;
    }
    .form-contact xinput[type="file"]{
        padding-left: 0;
        padding-right: 0;
    }
    .form-contact .input-file{
        overflow: hidden;
    }
    .form-contact textarea {
        min-height: 224px;
    }
    .form-contact .action input {
        border-radius: 0;
        background-color: #b18857;
        border-color: #b18857;
        color: #e9e4d0;
        border: 2px solid #b18857;
        transition: all ease .3s;
        font-size: 1.2857em;
        text-align: center;
        line-height: 1.2857em;
        padding: 0.75em 1.5em;
        height: auto;
    }
    .form-contact form {
        margin-bottom: 4em;
    }
    .form-contact button:hover {
        border-color: #7f6342;
        background-color: #7f6342;
        color: #e9e4d0;
    }
    .form-contact .action {
        text-align: right;
    }
    .address-container h3,
    .form-contact h3 {
        color: #504037;
        font-size: 1.666em;
        line-height: 1.2em;
        font-weight: 600;
        margin-bottom: 1.05em;
        padding-bottom: 2px;
    }
    .address-container h5 {
        color: #504037;
        font-size: 1.166em;
        line-height: 1.37em;
        font-weight: 600;
        margin-top: 0.76em;
        text-transform: uppercase;
        margin-bottom: 0;
    }
    .addresses {
        position: relative;
    }
    .addresses-item {
        padding-bottom: 1em;
    }
    .addresses-item p{
        margin-bottom: 0.5rem;
    }
    .address-container {
        color: #8b857d;
    }

    </style>
@endpush


@section('content')

    @include('includes.header')
    <div class="sub-header" style="background-image:url('{{url('/images/'.@$contact_us->image)}}')" @editableimg(params,contact_us,image)>
        <h1 @editable(params,contact_us,title)>
            {{$contact_us->title??'title'}}
        </h1>
        <ul class="breadcumb">
            <li>
                <a href="/">Inicio</a>
            </li>
        </ul>
    </div>
    <section class="map">
        <iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="{{$contact_us->iframe_map??'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3900.247117069169!2d-76.95599208518632!3d-12.163571991389546!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x4f1629d127cddf35!2sMaderera+SyC+Pizarro!5e0!3m2!1ses-419!2spe!4v1561321502925!5m2!1ses-419!2spe'}}"
                width="100%" height="385" frameborder="0" style="border:0" allowfullscreen></iframe>
    </section>
    <section class="home-section">
        <div class="container">
            <div class="row">
                <div class="six columns">
                    <form method="post" action="/sendmail" class="form-contact" enctype="multipart/form-data">
                        <div class="row">
                            <div class="twelve columns">
                                <h3 id="{{str_slug($contact_us->form_title??'title')}}" class="title" @editable(params,contact_us,form_title)>
                                    {{$contact_us->form_title??'title'}}
                                </h3>
                            </div>
                        </div>
                        <div class="row">
                            <div class="six columns">
                                <input name="name" type="text" placeholder="Nombre *" required>
                            </div>
                            <div class="six columns">
                                <input name="email" type="text" placeholder="E-mail *" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="twelve columns">
                                <textarea name="message" placeholder="Mensaje" required></textarea>
                                <div class="input-file">
                                    <input name="attach" type="file">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="twelve columns">
                                <div class="action">
                                    <input type="submit" value="Enviar Mensaje">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="six columns">
                    <div class="address-container">
                        <div class="row">
                            <div class="twelve columns">
                                <h3 id="{{str_slug($contact_us->address_title??'title')}}" class="title" @editable(params,contact_us,address_title)>
                                    {{$contact_us->address_title??'title'}}
                                </h3>
                                <div data-name="Info" class="addresses" @repeater(params,contact_us,addresses,INDEX)>
                                    @foreach(@$contact_us->addresses??[[]] as $key => $address)
                                    <div class="addresses-item" @repeateritem(params,contact_us,addresses,$key)>
                                        <h5 @editable(params,contact_us,addresses,$key,title)>
                                            {{@$address->title??'Officina'}}
                                        </h5>
                                        <p @editable(params,contact_us,addresses,$key,content)>
                                            {{@$address->content??'Dirección'}}
                                        </p>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>



    @include('includes.footer')

@endsection

