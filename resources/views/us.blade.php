@extends('layouts.default')

@section('title', 'Nosotros')

@push('styles')
<style>
    @media (max-width: 749px) {
        light home-section big-section {

        }
    }
</style>
@endpush

@section('content') 


    @include('includes.header')

    <div class="sub-header" style="background-image:url('{{url('/images/'.@$us->image)}}')" @editableimg(params,us,image)>
        <h1 @editable(params,us,title)>
            {{$us->title??'title'}}
        </h1>
        <ul class="breadcumb">
            <li>
                <a href="/">Inicio</a>
            </li>
            <li class="active" @editable(params,us,title)>
                {{@$us->title}}
            </li>
        </ul>
    </div>
    <section class="light home-section">
        <div class="container">
            <div>
                <div class="section-subtitle" @editable(params,us,intro)>
                    {{$us->intro??'intro'}}
                </div>
                <h3 id="{{str_slug($us->who_we_are->title??'title')}}" class="section-title" @editable(params,us,who_we_are,title)>
                    {{$us->who_we_are->title??'title'}}
                </h3>
            </div>
            @foreach($us->who_we_are->content??[''] as $content_key => $content)
                <p @editable(params,us,who_we_are,content,$content_key)>
                    {{$content??'content'}}
                </p>
            @endforeach        
        </div>
    </section>
    <div class="highlight">
        <div class="background" @editableimg(params,us,highlightimage)>
            <img src="{{url('/images/'.@$us->highlightimage)}}">
        </div>
        <div class="content" @editable(params,us,highlight)>
            {{$us->highlight??'highlight'}}
        </div>        
    </div>
    <section class="light home-section big-section">
        <div class="container">
            <div class="block-dark">
                <div id="{{str_slug($us->mission->title??'title')}}" class="title" @editable(params,us,mission,title)>
                    {{$us->mission->title??'title'}}
                </div>
                <p class="content" @editable(params,us,mission,content)>
                    {{$us->mission->content??'content'}}
                </p>
            </div>
        </div>
    </section>
    <div class="highlight">
        <div class="background" @editableimg(params,us,highlightimage2)>
            <img src="{{url('/images/'.@$us->highlightimage2)}}">
        </div>
        <div class="content" @editable(params,us,highlight2)>
            {{$us->highlight2??'highlight'}}
        </div>
    </div>
    <section class="light home-section big-section">
        <div class="container">
            <div class="block-dark">
                <div id="{{str_slug($us->vission->title??'title')}}" class="title" @editable(params,us,vission,title)>
                    {{$us->vission->title??'title'}}
                </div>
                <p class="content" @editable(params,us,vission,content)>
                    {{$us->vission->content??'content'}}
                </p>
            </div>
        </div>
    </section>

    @include('includes.footer')

@endsection

