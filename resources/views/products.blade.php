@extends('layouts.default')
@section('title', ucfirst(mb_strtolower($products->title)) )
@push('styles')
<style>
    .products.home-section {
        background-color:#e9e4d0
    }
    section.products {
        background-color: rgba(233,228,208,1);
    }
    .product-item {
        background-color: #f0eddf;
        margin-bottom: 3em;
    }
    .product-item-content {
        text-align: center;
        padding: 0 1em 1.5em;
        font-size: 13px;
        line-height: 1.6em;
    }
    .product-item-image {
        padding-bottom: 83.333%;
        display: block;
        position: relative;
        overflow: hidden;
        width: 100%;
        margin-bottom: 1.75em;
    }
    
    .product-item-image>img {
        position: absolute;
        height: 100%;
        object-fit: cover;
        width: 100%;
    }
    .product-item-title {
        font-size: 1em;
        font-weight: 400;
        line-height: 1.3em;
        margin-top: 0.8em;
        margin-bottom: 0.7em;
        padding: 0;
        max-height: 2.5em;
        text-transform: none;
        letter-spacing: 0;
        color: #504037;
    }
    
    .product-item-action {
        display: block;
        color: #fff;
        background-color: #504037;
        padding: 0.85em 2.58em 0.65em;
        font-weight: 500;
        text-transform: uppercase;
        margin-top: 1.25em;
        margin-bottom: 0;
    }
    .product-item-action:hover {
        background-color: #b18857;
        color: #fff;
    }

</style>
@endpush


@section('content') 


@include('includes.header')

<div class="sub-header" style="background-image:url('{{url('/images/'.@$products->image)}}')" @editableimg(params,products,image)>
    <h1 @editable(params,products,title)>
        {{$products->title??'title'}}
    </h1>
    <ul class="breadcumb">
        <li>
            <a href="/">Inicio</a>
        </li>
        <li class="active" @editable(params,products,title)>
            {{@$products->title}}
        </li>
    </ul>
</div>
<section class="products home-section">
    <div class="container">
        
        <div class="row">
            @foreach (@$products_list?$products_list:[[]] as $key => $item)
            <div class="three columns" data-refresh="1" @repeateritem(products,$key)>
                <div class="product-item">
                    <a class="product-item-image image-icon-link" href="{{@$item->action->url}}" @editableimg(products,$key,image_texture)>
                        <img src="{{url('images/'.@$item->image_texture)}}"/>
                    </a>
                    <div class="product-item-content">
                        <div class="product-item-title" @editable(products,$key,title)>
                            {{$item->title??'title'}}
                        </div>
                        <a class="product-item-action" href="{{"/productos/$key"}}">
                            {{$item->action->text??'Ver'}}
                        </a>
                    </div>
                </div>
            </div>
            @if(($loop->index+1) % 4 === 0)
        </div>
        <div class="row">
            @endif
            @endforeach
        </div>
    </div>
</section>

@include('includes.footer')

@endsection

