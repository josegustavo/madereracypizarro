@extends('layouts.default')
@section('title', ucfirst(mb_strtolower($sustentability->title)) )
@push('styles')
    <style>
        .call_to_action {
            background-color: #b18857;
            color: #FFF;
            margin-bottom: 2em;
        }
        .image-container {
            position: relative;
            padding-bottom: 80%;
            overflow: hidden;
        }
        .image-container img {
            width: 100%;
            max-width: 100%;
            height: 100%;
            position: absolute;
        }
        .call_to_action .left {
            padding: 4em 6em;
        }
        .call_to_action .columns {
            position: relative;
        }
        .call_to_action .title {
            font-size: 3.5em;
            line-height: 1.35em;
            font-weight: 600;
            margin-top: 0.54em;
            margin-bottom: 0.54em;
            letter-spacing: -1.06px;
        }
        .call_to_action .title:after {
            content: ' ';
            display: block;
            width: 40px;
            height: 2px;
            margin: 0.18em 0;
            background-color: #fff;
        }
        .call_to_action .desc {
            font-weight: 400;
            font-style: normal;
            margin-bottom: 0;
            margin-top: 1.1em;
            line-height: 1.3em;
            text-align: left;
            font-size: 1.4em;
        }

    </style>
@endpush


@section('content')


    @include('includes.header')

    <div class="sub-header" style="background-image:url('{{url('/images/'.@$sustentability->image)}}')" @editableimg(params,sustentability,image)>
        <h1 @editable(params,sustentability,title)>
            {{$sustentability->title??'title'}}
        </h1>
        <ul class="breadcumb">
            <li>
                <a href="/">Inicio</a>
            </li>
            <li class="active" @editable(params,sustentability,title)>
                {{@$sustentability->title}}
            </li>
        </ul>
    </div>
    <section class="home-section">
        <div class="container">
            <div>
                <div class="section-subtitle" @editable(params,sustentability,intro)>
                    {{$sustentability->intro??'intro'}}
                </div>
                <h3 class="section-title" @editable(params,sustentability,who_we_are_title)>
                    {{$sustentability->who_we_are_title??'title'}}
                </h3>
            </div>
            <div data-name="Bloque" @repeater(params,sustentability,blocks,INDEX)>
                @foreach (@$sustentability->blocks?$sustentability->blocks:[[]] as $key => $block)
                <div class="call_to_action" @repeateritem(params,sustentability,blocks,$key)>
                    <div class="row">
                        <div class="six columns left big-section">
                            <div class="title" @editable(params,sustentability,blocks,$key,title)>
                                {{$block->title??"Title"}}
                            </div>
                            <div class="desc" @editable(params,sustentability,blocks,$key,description)>
                                {{$block->description??"Description"}}
                            </div>
                        </div>
                        <div class="six columns right">
                            <div class="image-container" @editableimg(params,sustentability,blocks,$key,image)>
                                <img src="{{url('images/'.@$block->image)}}" />
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>

    </section>
    


    @include('includes.footer')

@endsection

