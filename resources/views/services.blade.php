@extends('layouts.default')
@section('title', ucfirst(mb_strtolower($services->title)) )
@push('styles')
    <style>
        .call_to_action {
            background-color: #b18857;
            color: #FFF;
            max-height: 513px;
        }
        .call_to_action .right>img {
            width: 100%;
            max-width: 100%;
        }
        .call_to_action .left {
            padding: 1em 2em;
        }
        .call_to_action .columns {
            position: relative;
        }
        .call_to_action .title {
            font-size: 3.5em;
            line-height: 1.35em;
            font-weight: 600;
            margin-top: 0.54em;
            margin-bottom: 0.54em;
            letter-spacing: -1.06px;
        }
        .call_to_action .title:after {
            content: ' ';
            display: block;
            width: 40px;
            height: 2px;
            margin: 0.18em 0;
            background-color: #fff;
        }
        .call_to_action .desc {
            font-weight: 400;
            font-style: normal;
            margin-bottom: 0;
            margin-top: 1.1em;
            line-height: 1.3em;
            text-align: left;
            font-size: 1.4em;
        }
        .call_to_action .action:hover {
            color: #f0eddf;
            background-color: #504037;
        }
        .call_to_action .action {
            color: #504037;
            background-color: #f0eddf;
            font-size: 1.166em;
            padding: 1em 3.3em;
            margin-top: 2em;
            line-height: 1.2857em;
            display: inline-block;
            text-align: center;
            font-weight: 500;
            text-transform: uppercase;
        }

        .service_list {
            margin-top: 8em;
            margin-bottom: 6em;
        }
        .service_item {
            width: 33.1%;
            margin: 0;
            word-wrap: break-word;
            min-height: 154px;
            display: inline-flex;
            position: relative;
        }
        .service_item .icon:hover {
            background-color: #b18857;
            color: #fff;
            border-color: #b18857;
        }
        .service_item .icon {
            width: 60px;
            height: 60px;
            display: block;
            float: left;
            color: #b18857;
            border-radius: 50%;
            background-position: center;
            background-size: cover;
            /*border: 1px dashed #b18857;*/
            -webkit-transition: all ease .3s;
            -moz-transition: all ease .3s;
            -ms-transition: all ease .3s;
            -o-transition: all ease .3s;
            transition: all ease .3s;
        }
        .service_list .title {
            color: #504037;
            text-transform: uppercase;
            letter-spacing: 0px;
            font-family: "Poppins", sans-serif;
            font-size: 0.6667em;
            line-height: 1.35em;
            font-weight: 600;
        }
        .service_list .content {
            margin-left: 70px;
        }
        .service_list h4 {
            margin: 0;
        }
        .service_list h4:after {
            content: ' ';
            display: block;
            width: 40px;
            height: 2px;
            background-color: #b18857;
        }
        .service_list .description {
            font-size: 13px;
            line-height: 1.6em;
            margin-top: 1.1em;
            overflow: hidden;
            margin-bottom: 0.5em;
            color: #8b857d;
        }


        .partner-list {
            text-align: center;
        }
        .partner-item {
            min-width: 170px;
            max-width: 170px;
            max-height: 170px;
            display: inline-block;
            position: relative;
            padding: 5px 20px;
        }
        .partner-item .img-container {
            width: 100%;
            padding-bottom: 100%;
            background-position: center;
            background-repeat: no-repeat;
            background-size: contain;
            position: relative;
            color:#fff;
        }

        @media (max-width: 749px) {
            .service_item {
                width: 100%;
            }
        }
    </style>
@endpush


@section('content')


    @include('includes.header')

    <div class="sub-header" style="background-image:url('{{url('/images/'.@$services->image)}}')" @editableimg(params,services,image)>
        <h1 @editable(params,services,title)>
            {{$services->title??'title'}}
        </h1>
        <ul class="breadcumb">
            <li>
                <a href="/">Inicio</a>
            </li>
            <li class="active" @editable(params,services,title)>
                {{@$services->title}}
            </li>
        </ul>
    </div>
    <section class="home-section">
        <div class="container">
            <div>
                <div class="section-subtitle" @editable(params,services,intro)>
                    {{$services->intro??'intro'}}
                </div>
                <h3 class="section-title" @editable(params,services,who_we_are_title)>
                    {{$services->who_we_are_title??'title'}}
                </h3>
            </div>
            <div class="call_to_action">
                <div class="row">
                    <div class="six columns left big-section">
                        <div class="title" @editable(params,services,call_to_action,title)>
                            {{$services->call_to_action->title??"Title"}}
                        </div>
                        <div class="desc" @editable(params,services,call_to_action,description)>
                            {{$services->call_to_action->description??"Description"}}
                        </div>
                        <a class="action" href="{{@$prod->url}}" @editable(params,services,call_to_action,action_text)>
                            {{$services->call_to_action->action_text??"action"}}
                        </a>
                    </div>
                    <div class="six columns right" @editableimg(params,services,call_to_action,image)>
                        <img src="{{url('images/'.@$services->call_to_action->image)}}" />
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row service_list">
                @foreach (@$services_list as $key => $service)
                <div class="service_item" @repeateritem(services,$key)>
                    <div>
                        <a class="icon" href="/" style="background-image:url('{{url('/images/'.@$service->icon)}}')" @editableimg(services,$key,icon)>

                        </a>
                        <div class="content">
                            <h4><a class="title" href="{{"/servicios/$key"}}" no-url="1" @editable(services,$key,title)>{{$service->title??"Title"}}</a></h4>
                            <p class="description" @editable(services,$key,description)>{{$service->description??"Description"}}</p>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>
    <section class="light home-section">
        <div class="container">
            <div>
                <div class="section-subtitle" @editable(params,services,partner_intro)>
                    {{$services->partner_intro??'intro'}}
                </div>
                <h3 class="section-title" @editable(params,services,partner_title)>
                    {{$services->partner_title??'title'}}
                </h3>
            </div>
            <div class="partner-list row" @repeater(params,services,partner_list,INDEX)>
                @foreach (@$services->partner_list?$services->partner_list:[[]] as $key => $partner)
                <div class="partner-item" @repeateritem(params,services,partner_list,$key)>
                    <div class="img-container" style="background-image:url('{{url('/images/'.@$partner->image)}}')" @editableimg(params,services,partner_list,$key,image)>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>



    @include('includes.footer')

@endsection

